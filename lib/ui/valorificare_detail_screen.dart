import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:flutter/services.dart';
import 'package:hostware_flutter/ui/valorificare_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

ScrollController scrollControllerValorificareDetail;
Timer _timer;
class ValorificareDetailScreen extends StatefulWidget {
  ArticolCategoryElem data;
  String period;

  ValorificareDetailScreen(ArticolCategoryElem data_,String period_, {Key key}) : super(key: key) {
    data = data_;
    period = period_;
  }

  @override
  ValorificareDetailState createState() => ValorificareDetailState(this.data, this.period);
}

class ValorificareDetailState extends State<ValorificareDetailScreen> {
//  List<ArticolElem> myDatas;
  List<ArticolElem> articolesList;
  List<double> vanzariWidths;
  List<double> castigWidths;
  ArticolCategoryElem data;
  String period;

  bool isVanzariOrdering = true;

  String totalVanzari;
  String totalCastig;
  double totalVanzariValue;
  double totalCastigValue;
  double sumTotalVanzari = 0;
  double sumTotalCastig = 0;

  ValorificareDetailState(this.data, this.period);

  @override
  void initState() {
    initData();

    scrollControllerValorificareDetail = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    if (_timer!=null) {
      _timer.cancel();
    }
    scrollControllerValorificareDetail.dispose();
    super.dispose();
  }

  void initData() {
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetVanzareArticole(context, url, period != null ? period : "", data != null ? data.articolCategoryId.toString() : "").then((response) {
        setState(() {
          if (response != null) {
            print('makeGetVanzareArticole hivas nem null ');
            totalVanzari = response.totalVanzare != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.totalVanzare) : "-";
            totalCastig = response.totalCastig != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.totalCastig) : "-";
            totalVanzariValue = response.totalVanzare;
            totalCastigValue = response.totalCastig;

            articolesList = response.articole;
            if (articolesList==null){
             articolesList = new List<ArticolElem>();
            }
            articolesList.sort((a, b) => a.vanzareBrut.compareTo(b.vanzareBrut));
            articolesList = new List.from(articolesList.reversed);

            vanzariWidths = new List<double>();
            castigWidths = new List<double>();

            for ( ArticolElem articolElem in articolesList) {
              vanzariWidths.add(0);
              castigWidths.add(0);
//                sumTotalVanzari += articolElem.vanzareBrut.abs();
//                sumTotalCastig += articolElem.castig.abs();
                if (articolElem.castig.abs() > sumTotalCastig) {
                  sumTotalCastig = articolElem.castig.abs();
                }
                if (articolElem.vanzareBrut.abs() > sumTotalVanzari) {
                  sumTotalVanzari = articolElem.vanzareBrut.abs();
                }
            }
            startTimer();
          } else {
            totalVanzari = "";
            totalCastig = "";
            articolesList = new List<ArticolElem>();
            this.callbackErr("server");          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void startTimer() {
    _timer = Timer.periodic(Duration(milliseconds: 300), (Timer t) {
      _timer.cancel();
      setState(() {
        articolesList.sort((a, b) => a.vanzareBrut.compareTo(b.vanzareBrut));
        articolesList = new List.from(articolesList.reversed);
        int i = 0;
        for ( ArticolElem articolElem in articolesList) {
          vanzariWidths[i] = (MediaQuery.of(context).size.width*(articolElem.vanzareBrut.abs()))/(sumTotalVanzari);
          i++;
        }
        List<ArticolElem> articolesList2 = new List.from(articolesList);
        articolesList2.sort((a, b) => a.castig.compareTo(b.castig));
        articolesList2 = new List.from(articolesList2.reversed);
        i = 0;
        for ( ArticolElem articolElem in articolesList2) {
          castigWidths[i] = (MediaQuery.of(context).size.width*(articolElem.castig.abs()))/(sumTotalCastig);
          i++;
        }
      });
    });
  }
  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {
          articolesList = new List<ArticolElem>();
          totalVanzari = "";
          totalCastig= "";
        });

      }
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    data==null?"":data.articolCategoryName,
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
              Container(
                height: 22,
                color: Theme.Colors.lightestGrey,
                child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                        onTap: () => _onTapOrderVanzari(true),
                        child:Container(
                      margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 1.0, right: 0.0),
                      color: isVanzariOrdering?Theme.Colors.HWrowGrey:Theme.Colors.HWwhite,
                        child: Align(
                         alignment: Alignment.center,
                         child: Padding(
                           padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                           child: Text("Vanzari", style: TextStyle(fontWeight: FontWeight.normal, color: isVanzariOrdering?Theme.Colors.HWwhite:Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                         )),
                    ))
                  ),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => _onTapOrderVanzari(false),
                          child:Container(
                        color: isVanzariOrdering?Theme.Colors.HWwhite:Theme.Colors.HWrowGrey,
                        margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 0.0, right: 1.0),
                        child: Align(
                           alignment: Alignment.center,
                           child: Padding(
                             padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                             child: Text("Castig", style: TextStyle(fontWeight: FontWeight.normal, color: isVanzariOrdering?Theme.Colors.HWBlackTextColor:Theme.Colors.HWwhite, fontSize: kFontSize12)),
                           )),
                      )))
                ]),
              ),
              Container(
                height: kMargin20,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWwhite,
              ),
      Container(
        height: kRowHeight23,
        child: new Container(
          color: Theme.Colors.HWwhite,
          height: kRowHeight23,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    isVanzariOrdering?"Total vanzari":"Total castig",
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin20),
                child: Container(
                  child: isVanzariOrdering&&totalVanzari==null?
                  Container(
                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                      width: kProgressSize,
                      height: kProgressSize,
                      child: CircularProgressIndicator(
                        backgroundColor: Theme.Colors.HWwhite,
                        strokeWidth: 1,
                      ))
                      : !isVanzariOrdering&&totalCastig==null?
                  Container(
                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                      width: kProgressSize,
                      height: kProgressSize,
                      child: CircularProgressIndicator(
                        backgroundColor: Theme.Colors.HWwhite,
                        strokeWidth: 1,
                      ))
                  :
                  Text(
                    isVanzariOrdering?totalVanzari:totalCastig,
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        color: Theme.Colors.HWrowGrey,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(),
          ),
          Expanded(
            flex: 4,
            child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                  child: Text("Vanzare brut", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                )),
          ),
          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Castig", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  )))
        ]),
      ),
      articolesList!=null?
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerValorificareDetail,
                    itemExtent: kRowHeight40,
                    itemCount: articolesList!=null?articolesList.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                              child: Stack(children: <Widget>[
                                new Align(
                                    alignment: Alignment.bottomLeft,
                                    child: AnimatedContainer(
                                      curve: Curves.ease,
                                      duration: new Duration(milliseconds: 800),
                                      decoration: BoxDecoration(
                                          color: isVanzariOrdering?(articolesList[index].vanzareBrut>=0?Theme.Colors.HWBlackTextColor:Theme.Colors.HWyellow):(articolesList[index].castig>-0?Theme.Colors.HWred:Theme.Colors.HWyellow),
                                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                          border: Border.all(
                                            color: isVanzariOrdering?(articolesList[index].vanzareBrut>=0?Theme.Colors.HWBlackTextColor:Theme.Colors.HWyellow):(articolesList[index].castig>-0?Theme.Colors.HWred:Theme.Colors.HWyellow),
                                            width: 1,
                                          )),
                                      height: 5,
                                      width: isVanzariOrdering?vanzariWidths[index]:castigWidths[index],
//                                      width: (MediaQuery.of(context).size.width*(isVanzariOrdering?articolesList[index].vanzareBrut.abs():articolesList[index].castig.abs()))/(isVanzariOrdering?(sumTotalVanzari):(sumTotalCastig)),
                                    )),
                                Center(
                                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                        child: Text(articolesList[index].articolName, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text(articolesList[index].getVanzariRoundedAsString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    ),
                                    Expanded(
                                        flex: 4,
                                        child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Padding(
                                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                              child: Text(articolesList[index].getCastigRoundedAsString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWred, fontSize: kFontSize14)),
                                            )))
                                  ]),
                                )
                              ]),
                            )),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          )):
      Container(
          margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWwhite,
            strokeWidth: 1,
          ))
    ])));
  }

  void _onTapOrderVanzari(bool vanzariClicked) {

    setState(() {
      if (vanzariClicked) {

//        myDatas = new List();
//        myDatas.add(new MyData3(text: "Articol 3" , value:"3.200.000,00", value2: "2.900.000,00"));
//        myDatas.add(new MyData3(text: "Articol 1" , value:"1.200.000,00", value2: "1.200.000,00"));
//        myDatas.add(new MyData3(text: "Articol 4" , value:"700.000,00", value2: "100.000,00"));
//        myDatas.add(new MyData3(text: "Articol 2" , value:"400.000,00", value2: "200.000,00"));
        articolesList.sort((a, b) => a.vanzareBrut.compareTo(b.vanzareBrut));
//        myDatas = new List();
//        myDatas.add(new MyData3(text: "Articol 3" , value:"3.200.000,00", valueDouble: 3200000.00, value2: "2.900.000,00", valueDouble2: 2900000.00 ));
//        myDatas.add(new MyData3(text: "Articol 1" , value:"1.200.000,00", valueDouble: 1200000.00, value2: "1.200.000,00", valueDouble2: 1200000.00 ));
//        myDatas.add(new MyData3(text: "Articol 4" , value:"700.000,00", valueDouble: 700000.00, value2: "100.000,00", valueDouble2: 100000.00 ));
//        myDatas.add(new MyData3(text: "Articol 2" , value:"400.000,00", valueDouble: 400000.00, value2: "200.000,00", valueDouble2: 200000.00 ));
        isVanzariOrdering = true;
      } else {
        articolesList.sort((a, b) => a.castig.compareTo(b.castig));
//        myDatas = new List();
//        myDatas.add(new MyData3(text: "Articol 3" , value:"3.200.000,00", value2: "2.900.000,00"));
//        myDatas.add(new MyData3(text: "Articol 1" , value:"1.200.000,00", value2: "1.200.000,00"));
//        myDatas.add(new MyData3(text: "Articol 2" , value:"400.000,00", value2: "200.000,00"));
//        myDatas.add(new MyData3(text: "Articol 4" , value:"700.000,00", value2: "100.000,00"));

//        myDatas = new List();
//        myDatas.add(new MyData3(text: "Articol 3" , value:"3.200.000,00", valueDouble: 3200000.00, value2: "2.900.000,00", valueDouble2: 2900000.00 ));
//        myDatas.add(new MyData3(text: "Articol 1" , value:"1.200.000,00", valueDouble: 1200000.00, value2: "1.200.000,00", valueDouble2: 1200000.00 ));
//        myDatas.add(new MyData3(text: "Articol 2" , value:"400.000,00", valueDouble: 400000.00, value2: "200.000,00", valueDouble2: 200000.00 ));
//        myDatas.add(new MyData3(text: "Articol 4" , value:"700.000,00", valueDouble: 700000.00, value2: "100.000,00", valueDouble2: 100000.00 ));

        isVanzariOrdering = false;


      }
      articolesList = new List.from(articolesList.reversed);

    });
  }


  Future<VanzareArticolDetailElem> makeGetVanzareArticole(BuildContext context, String basicUrl, String period, String categoryId) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/VanzareArticole"+"?period="+period+"&categoryID="+categoryId;
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' makeGetVanzareArticole response elot t '+url.toString());
      print('makeGetVanzareArticole  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('makeGetVanzareArticole  response elott response tostring>'+response.toString());
        VanzareArticolDetailElem vanzareArticolDetailElem = new VanzareArticolDetailElem();
        final responseJson = json.decode(response.body);
        vanzareArticolDetailElem = new VanzareArticolDetailElem.fromJson(responseJson);

        return vanzareArticolDetailElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }

    return null;
  }

}

class VanzareArticolDetailElem {
  final double totalVanzare;
  final double totalCastig;
  final List<ArticolElem> articole;


  VanzareArticolDetailElem({this.totalVanzare, this.totalCastig, this.articole});

  factory VanzareArticolDetailElem.fromJson(Map<String, dynamic> parsedJson) {
    var articoles = parsedJson['Articole']!=null?  parsedJson['Articole'] as List:null;
    List<ArticolElem> articolesList = articoles!=null?articoles.map((i) => ArticolElem.fromJson(i)).toList():null;
    return VanzareArticolDetailElem(
        totalVanzare: parsedJson['Total_Vanzare'],
        totalCastig: parsedJson['Total_Castig'],
        articole: articolesList
    );
  }

}

class ArticolElem {

  final String articolName;
  final double cantitate;
  final double vanzareBrut;
  final double castig;

  ArticolElem(
      {this.articolName,
        this.cantitate,
        this.vanzareBrut,
        this.castig});

  factory ArticolElem.fromJson(Map<String, dynamic> parsedJson) {
    return ArticolElem(
        articolName: parsedJson['Articol_Name'],
        cantitate: parsedJson['Cantitate'],
        vanzareBrut: parsedJson['Vanzare_Brut'],
        castig: parsedJson['Castig']
    );
  }

  String getVanzariAsString(){
    if (vanzareBrut == null){
      return "";
    }
    return vanzareBrut.toString();
  }
  String getVanzariRoundedAsString(){
    if (vanzareBrut == null){
      return "";
    }
    return NumberFormat.currency(locale: 'eu', symbol: '').format(vanzareBrut);
  }
  String getCastigAsString(){
    if (castig == null){
      return "";
    }
    return castig.toString();
  }
  String getCastigRoundedAsString(){
    if (castig == null){
      return "";
    }
    return NumberFormat.currency(locale: 'eu', symbol: '').format(castig);
  }
}
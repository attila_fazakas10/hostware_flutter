import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:flutter/services.dart';

ScrollController scrollControllerOptimizareSettings;

class OptimizareSettingsScreen extends StatefulWidget {

  OptimizareSettingsScreen({Key key}) : super(key: key) {

  }

  @override
  OptimizareSettingsState createState() => OptimizareSettingsState();
}

class OptimizareSettingsState extends State<OptimizareSettingsScreen> {
  List<MyDataOptimizareSettings> myDatas;

  int selectedTab = 1;

  OptimizareSettingsState();

  @override
  void initState() {
    initData();

    scrollControllerOptimizareSettings = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerOptimizareSettings.dispose();
    super.dispose();
  }

  void initData() {
    setState(() {
      myDatas = new List();
      myDatas.add(new MyDataOptimizareSettings(text: "Unitatea" , category: "", checked: false));
      myDatas.add(new MyDataOptimizareSettings(text: "Restaurant 1" , category: "unitatea", checked:false));
      myDatas.add(new MyDataOptimizareSettings(text: "Hotel 2" , category: "unitatea", checked:true));
      myDatas.add(new MyDataOptimizareSettings(text: "Restaurant hotel 3" , category: "unitatea", checked:false));
      myDatas.add(new MyDataOptimizareSettings(text: "Hotel restaurant 4" , category: "unitatea", checked:false));

      myDatas.add(new MyDataOptimizareSettings(text: "Din categoria" , category: "", checked: false));
      myDatas.add(new MyDataOptimizareSettings(text: "Cat. 4 (pl. Fresh Food)" , category: "categoria", checked:false));
      myDatas.add(new MyDataOptimizareSettings(text: "Cat. 1 (pl. Pizza)" , category: "categoria", checked:false));
      myDatas.add(new MyDataOptimizareSettings(text: "Cat. 2  (pl. Healthy Dishes)" , category: "categoria", checked:true));
      myDatas.add(new MyDataOptimizareSettings(text: "Cat. 3 (pl. Starters)" , category: "categoria", checked:false));
      myDatas.add(new MyDataOptimizareSettings(text: "Cat. 4 (pl. Fresh Food)" , category: "categoria", checked:false));
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Organizare profit",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
              Container(
                height: kRowHeight23,
                child: new Container(
                  color: Theme.Colors.HWwhite,
                  height: kRowHeight23,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                      Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: kMargin20),
                        child: Container(
                          child: Text(
                            "Arata datele pentru perioada ",
                            style: TextStyle(color: Theme.Colors.HWrowGrey, fontWeight: FontWeight.normal, fontSize: kFontSize14),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: kRowHeight23,
                color: Theme.Colors.HWlightestGrey,
                child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                        onTap: () => _onTapOrder(1),
                        child:Container(
                      margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 1.0, right: 0.0),
                      color: (selectedTab==1)?Theme.Colors.HWlightestGrey:Theme.Colors.HWwhite,
                        child: Align(
                         alignment: Alignment.center,
                         child: Padding(
                           padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                           child: Text("Ultima luna", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                         )),
                    ))
                  ),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => _onTapOrder(2),
                          child:Container(
                        color: (selectedTab==2)?Theme.Colors.HWlightestGrey:Theme.Colors.HWwhite,
                        margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 1.0, right: 1.0),
                        child: Align(
                           alignment: Alignment.center,
                           child: Padding(
                             padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                             child: Text("Ultimele 6 luni", style: TextStyle(fontWeight: FontWeight.normal, color:Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                           )),
                      ))),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => _onTapOrder(3),
                          child:Container(
                            color: (selectedTab==3)?Theme.Colors.HWlightestGrey:Theme.Colors.HWwhite,
                            margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 0.0, right: 1.0),
                            child: Align(
                                alignment: Alignment.center,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                  child: Text("Ultimele 12 luni", style: TextStyle(fontWeight: FontWeight.normal, color:Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                                )),
                          )))
                ]),
              ),



      Expanded(
          flex: 1,
          child: Container(
            color: Theme.Colors.HWwhite,
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerOptimizareSettings,
//                    itemExtent: kRowHeight40,
                    itemCount: myDatas.length,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: InkWell(
                      onTap: () => _onTapItem(context, myDatas[index]),
                      child:Container(
                              height: myDatas[index].category==""?kRowHeight60:kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: myDatas[index].category==""? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin20),
                              child: Stack(children: <Widget>[
                                new Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                      "assets/img/check.png",
                                      width: myDatas[index].checked?20.0:0.0,
                                      height: 20.0,
//                                      fit: BoxFit.cover,
                                    )),

                                Align(
                                    alignment: myDatas[index].category==""?Alignment.bottomLeft:Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(top: 0.0, bottom: myDatas[index].category==""?kMargin10:0.0, left: kMargin10, right: kMargin10),
                                      child: Text(myDatas[index].text, style: TextStyle(fontWeight: myDatas[index].checked?FontWeight.bold:FontWeight.normal, color: myDatas[index].category==""? Theme.Colors.HWrowGrey:Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                    )),
                              ]),
                            ))),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
    ])));
  }

  void _onTapOrder(int tabSel) {

    setState(() {
      selectedTab = tabSel;
    });
  }


  void _onTapItem(BuildContext context, MyDataOptimizareSettings data) {

    setState(() {
      for (int i = 0; i < myDatas.length; i++) {
        if (myDatas[i].category !="" && myDatas[i].category == data.category){
          myDatas[i].checked = false;
        }
      }
      if (data.category!="") {
        data.checked = true;
      }

    });


  }


}

class MyDataOptimizareSettings {
  String text;
  String category;
  bool checked;

  MyDataOptimizareSettings({this.text, this.category, this.checked});
}

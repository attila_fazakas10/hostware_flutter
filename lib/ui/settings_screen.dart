import 'dart:async';
import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:flutter/services.dart';

import 'package:http/http.dart' as http;

ScrollController scrollControllerSettings;
Timer _timer;
int _start = 3;
class SettingsScreen extends StatefulWidget {
  @override
  SettingsState createState() => SettingsState();
}

class SettingsState extends State<SettingsScreen> {
  bool locked = true;
  bool isValidationLabelVisible = false;
  bool isProgressVisible = false;
  bool isValidServer = false;
  String _serverName = "";
  String _port = "";
  String _apiKey = "";
  TextEditingController serverNameFieldController = new TextEditingController();
  TextEditingController portFieldController = new TextEditingController();
  TextEditingController apiKeyFieldController = new TextEditingController();
  bool needDialog = false;
  double _height = 0.0;
  @override
  void initState() {
    scrollControllerSettings = ScrollController(initialScrollOffset: 84);
    initData();

    super.initState();
  }

  @override
  void dispose() {
    print('dispose settings:');
    if (_timer!=null) {
      print('dispose settings cancel timer:');
      _timer.cancel();
      _start = 3;
    }
    scrollControllerSettings.dispose();
    super.dispose();
  }

  void initData() {
    SharedPreferencesHelper.getServerName().then((val) {
      setState(() {
        _serverName = val;
        serverNameFieldController = TextEditingController(text: _serverName);
      });
    });
    SharedPreferencesHelper.getPort().then((val) {
      setState(() {
        _port = val;
        portFieldController = TextEditingController(text: _port);
      });
    });
    SharedPreferencesHelper.getApiKey().then((val) {
      setState(() {
        _apiKey = val;
        apiKeyFieldController = TextEditingController(text: _apiKey);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(

        child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Setari",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Expanded(
          flex: 2,
          child: Container(
              color:Theme.Colors.HWwhite,
            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kRowHeight40, right: kRowHeight40),
            width: double.infinity,
              child: SingleChildScrollView(
                controller: scrollControllerSettings,
                child: Column(
              children: <Widget>[
                Container(
                  height: kMargin30,
                ),
                Text("Pentru schimbarea parametrilor de conectare apasati iconul lacat.", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14)),
                Container(
                  height: kMargin10,
                ),
                new Align(
                  alignment: Alignment.centerLeft,
                  child: new IconButton(
                    icon: Image.asset(
                      locked ? "assets/img/lakat.png" : "assets/img/unlakat.png",
                      width: 35.0,
                      height: 35.0,
                    ),
                    onPressed: () {
                      setState(() {
                        locked = !locked;
                        isValidationLabelVisible = false;
                        if (locked) {
                          SharedPreferencesHelper.setServerName(_serverName);
                          SharedPreferencesHelper.setPort(_port);
                          SharedPreferencesHelper.setApiKey(_apiKey);
                        }
                      });
                    },
                  ),
                ),
                Container(
                  height: kMargin10,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                       flex:2,
                            child:
                            new Container(
                              alignment: Alignment.centerRight,
                              child: Text("IP/Server name:", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14)),
                            )
                        ),
                    Expanded(
                        flex: 3,
                        child:
                            new Container(
                              height: kRowHeight35,
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: 0.0),
                              child: TextField(
                                onChanged: (text) {
                                  setState(() {
                                    isValidationLabelVisible = false;
                                    _serverName = text;
                                  });
                                },
                                controller: serverNameFieldController,
                                style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14),
                                autofocus: false,
                                obscureText: locked?true:false,
                                readOnly: locked?true:false,
                                decoration: new InputDecoration(
                                  contentPadding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: kMargin20),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Theme.Colors.HWGreyBorderColor, width: 1.0),
                                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Theme.Colors.HWGreyBorderColor, width: 1.0),
                                    borderRadius: BorderRadius.all(Radius.circular(0.0)),
                                  ),
                                  hintText: '',
                                ),),
                            ))
                  ],
                ),
                Container(
                  height: kMargin10,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        flex:2,
                        child:
                        new Align(
                          alignment: Alignment.centerRight,
                          child: Text("Port:", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14)),
                        )
                    ),
                    Expanded(
                        flex: 3,
                        child:
                        new Container(
                          height: kRowHeight35,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: 0.0),

                          child: TextField(
                            onChanged: (text) {
                            setState(() {
                              isValidationLabelVisible = false;
                              _port = text;
                            });
                          },
                            controller:portFieldController,style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14),
                            autofocus: false,
                            obscureText: locked?true:false,
                            readOnly: locked?true:false,
                            decoration: new InputDecoration(
                              contentPadding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: kMargin20),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Theme.Colors.HWGreyBorderColor, width: 1.0),
                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Theme.Colors.HWGreyBorderColor, width: 1.0),
                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                              ),
                              hintText: '',
                            ),),
                        ))
                  ],
                ),
                Container(
                  height: kMargin10,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        flex:2,
                        child:
                        new Align(
                          alignment: Alignment.centerRight,
                          child: Text("API Key:", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14)),
                        )
                    ),
                    Expanded(
                        flex: 3,
                        child:
                        new Container(
                          height: kRowHeight35,
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: 0.0),
                          child: TextField(
                            onChanged: (text) {
                              setState(() {
                                isValidationLabelVisible = false;
                                _apiKey = text;
                              });
                            },
                            controller: apiKeyFieldController,
                            style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14),
                            autofocus: false,
                            obscureText: locked?true:false,
                            readOnly: locked?true:false,
                            decoration: new InputDecoration(
                              contentPadding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: kMargin20),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Theme.Colors.HWGreyBorderColor, width: 1.0),
                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Theme.Colors.HWGreyBorderColor, width: 1.0),
                                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                              ),
                              hintText: '',
                            ),),
                        ))
                  ],
                ),
                Container(
                  height: kRowHeight35,
                ),
                  new InkWell(
                    onTap: () => _onTapTest(context),
                    child:new Container(
                    alignment: Alignment.centerRight,
                    child: Container(
                      decoration: BoxDecoration(
                          color: needDialog?Theme.Colors.HWlightGrey:Theme.Colors.HWrowGrey,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(
                            color: needDialog?Theme.Colors.HWlightGrey:Theme.Colors.HWrowGrey,
                            width: 1,
                          )),
                      height: locked?0:kRowHeight35,
                      width: (MediaQuery.of(context).size.width*0.4),
                      child: Align(
                        alignment: Alignment.center,child:Text("Testare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),)
                    )),),
                Container(
                    margin: EdgeInsets.only(top: kMargin10, bottom: kMargin4, left: 0.0, right: 0.0),
                    width: kProgressSize,
                    height: isProgressVisible?kProgressSize:0,
                    child: CircularProgressIndicator(
                      backgroundColor: Theme.Colors.HWlightGrey,
                      strokeWidth: 1,
                    )),

              ],
            )),
          )),
         /* Expanded(
            flex: 1,
            child: */ Container(

                margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),

                child: AnimatedContainer(
                  curve: Curves.linearToEaseOut,
                  duration: new Duration(milliseconds: 500),
                  height: _height,

//                    height: _height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Theme.Colors.HWwhite,
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        border: Border.all(
                          color: Theme.Colors.HWGreyBorderColor,
                          width: 1,
                        )),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        color: Theme.Colors.HWwhite,
                        height: _height,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: kMargin10, bottom: 0.0, left: 0.0, right: 0.0),
                              child: new Align(

                                  alignment: Alignment.topCenter,
                                  child: Image.asset(isValidServer? "assets/img/check_circle.png":"assets/img/alert_circle.png"
                                    ,
                                    width: 76.0,
                                    height: 76.0,
//                                      fit: BoxFit.cover,
                                  )),
                            ),
                            Expanded(
                              child:Container(
                                  margin: EdgeInsets.only(top: 0.0, bottom: kMargin10, left: 0.0, right: 0.0),
                                  child: new Align(
                                    alignment: Alignment.center,
                                    child: Text(isValidServer? "Succes":"Eșec", textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize24)),
                                  )),
                            ),
                            Expanded(
                              child:Container(
                                  margin: EdgeInsets.only(top: 0.0, bottom: kMargin10, left: 0.0, right: 0.0),
                                  child: new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Text(isValidServer? "Comunicatia cu server\na fost realizata":"Comunicatia cu server\nnu a fost realizata", textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWrowGrey, fontSize: kFontSize14)),
                                  )),
                            )

                          ],
                        ),
                      ),
                    ))


            ),
//          )
    ])));
  }

  _onTapTest(BuildContext context) {
    if(!needDialog) {
      setState(() {
        isProgressVisible = true;
      });
      _testCallSerialNr().then((val) =>
          setState(() {
            isProgressVisible = false;
            isValidationLabelVisible = true;
            if (val != null) {
              isValidServer = true;
              needDialog = true;
              _height = 175;
              startTimer();
            } else {
              needDialog = true;
              _height = 175;
              isValidServer = false;
              startTimer();
            }
          }));
    }
  }

  void startTimer() {
    print('======>start: ' + _start.toString());
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      if (_start < 1) {
        print('======>start: < 1 :' + _start.toString());
        _timer.cancel();
        setState(() {
          needDialog = false;
          _height = 0.0;
          _start = 3;
        });
      } else {
        print('======>start: > 1 :' + _start.toString());
        _start = _start - 1;
      };
    });
  }
  Future<Object> _testCallSerialNr() async {

    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          _serverName+":"+_port+BASIC_PATH+"/SerialNr";
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print('111111 HW SerialNr  ' + url);
      if (response.statusCode == 200) {
        print('200 HW SerialNr ' );
        final items = json.decode(response.body);

        return items;
      } else {
        print('HW SerialNr ' + response.statusCode.toString());
        throw Exception('Failed to load internet');
      }
    } on TimeoutException  catch (_) {
      print('Timeout SerialNr??? ');
      // A timeout occurred.
    } on Exception catch (_) {
      print('Exception SerialNr????'+_.toString());
      // A timeout occurred.
    } catch (exception){
      print('SEVERHIBAAA SerialNr');
      print('SEVERHIBAAA SerialNr:'+exception.toString());
    }

    return null;



  }

}

import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/parteneri_detail_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:hostware_flutter/ui/parteneri_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'package:flutter/services.dart';

import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:date_format/date_format.dart';
import 'package:http/http.dart' as http;

ScrollController scrollControllerParteneriDetailMore;
Timer _timer;
class ParteneriDetailMoreScreen extends StatefulWidget {
  NoteElem noteElem;
  int partnerId;
  String partnerName;


  ParteneriDetailMoreScreen(NoteElem noteElem_,int partnerId_,String partnerName_, {Key key}) : super(key: key) {
    noteElem = noteElem_;
    partnerId = partnerId_;
    partnerName = partnerName_;
  }

  @override
  ParteneriDetailMoreState createState() => ParteneriDetailMoreState(this.noteElem,this.partnerId,this.partnerName);
}

class ParteneriDetailMoreState extends State<ParteneriDetailMoreScreen> {
  NoteElem noteElem;
  int partnerId;
  String partnerName;
  NotaDetailListElem notaDetailListElem;
  NotaDetailElem notaDetailElem;
  double total;
  double minusTotal = 0;
  double maxArticolValue;
  List<double> articoleWidths;

  ParteneriDetailMoreState(this.noteElem,this.partnerId,this.partnerName );

  @override
  void initState() {
    initData();

    scrollControllerParteneriDetailMore = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    if (_timer!=null) {
      _timer.cancel();
    }
    scrollControllerParteneriDetailMore.dispose();
    super.dispose();
  }

  void initData() {
    setState(() {
      total = noteElem!=null?noteElem.value:0;


    });
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetNotaDetailsList(context, url).then((response) {
        setState(() {
          if (response != null) {
            notaDetailListElem = response;
            if (notaDetailListElem != null && notaDetailListElem.note != null && notaDetailListElem.note.length > 0 && notaDetailListElem.note[0].value != null) {
              notaDetailElem = notaDetailListElem.note[0];
              maxArticolValue = 0;
              articoleWidths = new List<double>();
              for (PartenerArticolElem articolElem in notaDetailElem.articole) {
                articoleWidths.add(0);
                if (articolElem.vanzareBrut < 0) {
                  minusTotal += articolElem.vanzareBrut;
                }
                if (articolElem.vanzareBrut> maxArticolValue) {
                  maxArticolValue = articolElem.vanzareBrut;
                }
              }
              print('maxArticolValue===============: ' + maxArticolValue.toString());
              print('minusTotal===============: ' + minusTotal.toString());
              startTimer();
              print('notaDetailListElem.note[0].value.toString(): ' + notaDetailListElem.note[0].value.toString());
            }
          } else {
            print('initstate hivas  null ');
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void startTimer() {
    _timer = Timer.periodic(Duration(milliseconds: 300), (Timer t) {
      _timer.cancel();
      setState(() {

        if (notaDetailListElem != null && notaDetailListElem.note != null && notaDetailListElem.note.length > 0 && notaDetailListElem.note[0].value != null) {
          notaDetailElem = notaDetailListElem.note[0];
          int i = 0;
          for (PartenerArticolElem articolElem in notaDetailElem.articole) {
            articoleWidths[i] = articolElem.vanzareBrut;
            i++;
          }
        }
      });
    });
  }
  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {

          notaDetailElem = new NotaDetailElem();
        });

      }
    });
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    partnerName,
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        child: new Container(
          color: Theme.Colors.HWwhite,
          height: kRowHeight23,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    noteElem!=null?convertDateFromString(noteElem.timestamp):"",
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              )
            ],
          ),
        ),
      ),

      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        color: Theme.Colors.HWrowGrey,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(),
          ),

          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Cantitate", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  ))),

          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Valoare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  )))
        ]),
      ),
      notaDetailElem!=null?
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerParteneriDetailMore,
                    itemExtent: kRowHeight40,
                    itemCount: (notaDetailElem!=null && notaDetailElem.articole!=null )?  notaDetailElem.articole.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                              child: Stack(children: <Widget>[

                                new Align(
                                    alignment: Alignment.bottomLeft,
                                    child: AnimatedContainer(
                                      curve: Curves.ease,
                                      duration: new Duration(milliseconds: 800),
                                      decoration: BoxDecoration(
                                          color: Theme.Colors.HWred,
                                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                          border: Border.all(
                                            color: Theme.Colors.HWred,
                                            width: 1,
                                          )),
                                      height: 5,
//                                      width: (total!=null && (notaDetailElem!=null && notaDetailElem.articole!=null && notaDetailElem.articole[index]!=null && notaDetailElem.articole[index].vanzareBrut!=null  && notaDetailElem.articole[index].vanzareBrut>0 ))?(MediaQuery.of(context).size.width*(notaDetailElem.articole[index].vanzareBrut))/(total-minusTotal):0,
                                      width: ((articoleWidths!=null && articoleWidths[index]!=null && articoleWidths.length>index  && articoleWidths[index] >0 && maxArticolValue > 0 ))?(MediaQuery.of(context).size.width*(articoleWidths[index]))/(maxArticolValue-minusTotal):0,
                                    )),
                                Center(
                                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                        child: Text((notaDetailElem!=null && notaDetailElem.articole!=null && notaDetailElem.articole[index]!=null)?notaDetailElem.articole[index].articolName:"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text((notaDetailElem!=null && notaDetailElem.articole!=null && notaDetailElem.articole[index]!=null && notaDetailElem.articole[index].cantitate!=null)?notaDetailElem.articole[index].cantitate.toString():"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text((notaDetailElem!=null && notaDetailElem.articole!=null && notaDetailElem.articole[index]!=null && notaDetailElem.articole[index].vanzareBrut!=null)?notaDetailElem.articole[index].vanzareBrut.toString():""+" lei", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    )
                                  ]),
                                )
                              ]),
                            )),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
          :
      Container(
          margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWwhite,
            strokeWidth: 1,
          ))
    ])));
  }


  Future<NotaDetailListElem> makeGetNotaDetailsList(BuildContext context, String basicUrl) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/NotaDetails"+"?partnerID="+partnerId.toString()+"&nota_date="+noteElem.timestamp;
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' makeGetNotaDetailsList response elot t '+url.toString());
      print('makeGetNotaDetailsList  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('makeGetNotaDetailsList  response elott response tostring>'+response.toString());
        NotaDetailListElem notaDetailListElem = new NotaDetailListElem();
        final responseJson = json.decode(response.body);
        notaDetailListElem = new NotaDetailListElem.fromJson(responseJson);

        return notaDetailListElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }


    return null;
  }

  String convertDateFromString(String strDate){
    DateFormat inputFormat = DateFormat("dd/MM/yyyy hh:mm:ss a");
    DateTime dateTime = inputFormat.parse(strDate);
    print('DATE:::::'+formatDate(dateTime, [yyyy, '/', mm, '/', dd, ' ', hh, ':', nn, ':', ss]));
    return formatDate(dateTime, [yyyy, '-', mm, '-', dd,' ',  hh, ':', nn]);
  }

}

class NotaDetailListElem {
  final List<NotaDetailElem> note;

  NotaDetailListElem({
    this.note,
  });

  factory NotaDetailListElem.fromJson(List<dynamic> parsedJson) {

    List<NotaDetailElem> note = new List<NotaDetailElem>();
    note = parsedJson.map((i)=>NotaDetailElem.fromJson(i)).toList();

    return new NotaDetailListElem(
        note: note
    );
  }
}


class NotaDetailElem{
  final String partnerName;
  final String timestamp;
  final double value;
  final List<PartenerArticolElem>  articole;

  NotaDetailElem({
    this.partnerName,
    this.timestamp,
    this.value,
    this.articole
  }) ;

  factory NotaDetailElem.fromJson(Map<String, dynamic> parsedJson){
    var articoles = parsedJson['Articole']!=null?  parsedJson['Articole'] as List:null;
    List<PartenerArticolElem> articoleList = articoles!=null? articoles.map((i) => PartenerArticolElem.fromJson(i)).toList():null;
    return new NotaDetailElem(
        partnerName: parsedJson['Partner_Name'].toString(),
        timestamp: parsedJson['Timestamp'].toString(),
        value: parsedJson['Value'],
        articole: articoleList
    );
  }

  String getValueRoundedAsString(){
    if (value == null){
      return "";
    }
    return double.parse(value.toStringAsFixed(2)).toString();
  }
}
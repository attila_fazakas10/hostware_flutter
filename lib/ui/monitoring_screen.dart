import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:hostware_flutter/ui/monitoring_detail_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;


class MonitoringScreen extends StatefulWidget {
  @override
  MonitoringState createState() => MonitoringState();
}

class MonitoringState extends State<MonitoringScreen> {
  List<MonitoringMasaElem> myMese;
  TextEditingController controller = new TextEditingController();
//  String filter;

  @override
  void initState() {
    initData();
    super.initState();
  }

  void initData() {
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetMonitoringList(context, url).then((response) {
        setState(() {
          if (response != null) {
            myMese = response.mese;
            if (myMese==null){
              myMese =  new List<MonitoringMasaElem>();
            }
          } else {
            print('initstate hivas  null ');
            myMese = new List<MonitoringMasaElem>();
            this.callbackErr("server");
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {
          myMese = new List<MonitoringMasaElem>();
        });

      }
    });
  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  // Init state logic
  Future initStateCustome() async {}

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Monitoring",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              )
            ],
          ),
        ),
      ),

      Container(
        height: kMargin4,
        color: Theme.Colors.HWwhite,
      ),
      myMese!=null&& myMese.length > 0?
      new Expanded(
          child: new ListView.builder(
        itemCount: myMese != null ? myMese.length : 0,
        itemBuilder: (BuildContext context, int index) {
          return buildRow(myMese[index]);
//          return filter == null || filter == "" ? buildRow(myDatas[index]) : myDatas[index].text.toLowerCase().contains(filter.toLowerCase()) ? buildRow(myDatas[index]) : new Container();
        },
      )):
          myMese!=null?
          Column (
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: kMargin30, bottom: kMargin30, left: kMargin30, right: kMargin30),
                  child: new Image( image: new AssetImage("assets/empty_mese.png"))),
              Text("Momentan nu este nici\no masa deschisa", textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWrowGrey, fontSize: kFontSize24))
            ],
          )
          :
          Container(
              margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
              width: kProgressSize,
              height: kProgressSize,
              child: CircularProgressIndicator(
                backgroundColor: Theme.Colors.HWwhite,
                strokeWidth: 1,
              ))

    ]));
  }

  Container buildRow(MonitoringMasaElem masa) {
    return Container(
        padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
        child: new InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MonitoringDetailScreen(masa)),
              );
            },
            child: Container(
                height: kBarHeight,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWrowGrey,
                padding: EdgeInsets.only(top: 0.0, bottom: 1.0, left: kMargin4, right: 1.0),
                child: Stack(
                  children: <Widget>[
                    new Align(
                        alignment: Alignment.bottomRight,
                        child: Image.asset(
                          "assets/img/polygon.png",
                          width: 18.0,
                          height: 18.0,
//                                      fit: BoxFit.cover,
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[

                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("Masa", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                              ),
                              Align(
                                  alignment: Alignment.bottomLeft,child: Text((masa!=null&& masa.masa!=null)?masa.masa.toString():"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                            ])),
                        Expanded(
                            flex: 3,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[

                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("Deschis la", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                              ),
                              Align(
                                  alignment: Alignment.bottomLeft,child: Text((masa!=null&& masa.deschisLa!=null)?masa.getDeschisLaFormatted():"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                            ])),
                        Expanded(
                            flex: 3,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[

                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("Oaspeti", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                              ),
                              Align(
                                  alignment: Alignment.bottomLeft,child: Text((masa!=null&& masa.nrOaspeti!=null)?masa.nrOaspeti.toString():"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                            ])),
                        Expanded(
                            flex: 4,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                  child:Text("Valoare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                )
                              ),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: Container(
                                      padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                      child:Text((masa!=null)?masa.getValoareFormatted()+" lei":"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24)))),
                            ])),
                      ],
                    )
                  ],
                ))));
  }


  Future<MonitoringListElem> makeGetMonitoringList(BuildContext context, String basicUrl) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/Monitoring";
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' Monitoring response elot t '+url.toString());
      print('Monitoring  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('Monitoring  response elott response tostring>'+response.toString());
        MonitoringListElem monitoringListElem = new MonitoringListElem();
        final responseJson = json.decode(response.body);
        monitoringListElem = new MonitoringListElem.fromJson(responseJson);

        return monitoringListElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }


    return null;
  }


}

class MonitoringListElem {
  final List<MonitoringMasaElem> mese;

  MonitoringListElem({
    this.mese,
  });

  factory MonitoringListElem.fromJson(List<dynamic> parsedJson) {

    List<MonitoringMasaElem> mese = new List<MonitoringMasaElem>();
    mese = parsedJson.map((i)=>MonitoringMasaElem.fromJson(i)).toList();

    return new MonitoringListElem(
        mese: mese
    );
  }
}

class MonitoringMasaElem{
  final int masa;
  final String deschisLa;
  final int nrOaspeti;
  final String ospatarName;
  final String valoare;
  final List<MonitoringArticolElem> articole;

  MonitoringMasaElem({
    this.masa,
    this.deschisLa,
    this.nrOaspeti,
    this.ospatarName,
    this.valoare,
    this.articole
  }) ;

  factory MonitoringMasaElem.fromJson(Map<String, dynamic> parsedJson){
    var articoles = parsedJson['articole']!=null? parsedJson['articole'] as List:null;
    List<MonitoringArticolElem> articolesList = articoles!=null?articoles.map((i) => MonitoringArticolElem.fromJson(i)).toList():null;
    return new MonitoringMasaElem(
      masa: parsedJson['masa'],
      deschisLa: parsedJson['deschis_la'].toString(),
      nrOaspeti: parsedJson['nr_oaspeti'],
      ospatarName: parsedJson['ospatar_name'].toString(),
      valoare: parsedJson['valoare'].toString(),
      articole: articolesList
    );
  }
  String getDeschisLaFormatted(){
    if (deschisLa == null){
      return "";
    }
    if (deschisLa.contains("T") && (deschisLa.indexOf("T")+6) < deschisLa.length){
      String dL = deschisLa.substring(deschisLa.indexOf("T")+1, deschisLa.indexOf("T")+6);
      return dL;
    }
    return deschisLa;
  }

  String getValoareFormatted(){
    if (valoare == null){
      return "";
    }
    if (valoare.contains(".") && (valoare.indexOf(".")+3) <= valoare.length){
      String v = valoare.substring(0, valoare.indexOf(".")+3);
      return v;
    }
    return valoare;
  }

}

class MonitoringArticolElem {
  final String articolName;
  final int cantitate;
  final double valoare;

  MonitoringArticolElem(
      {this.articolName,
        this.cantitate,
        this.valoare});

  factory MonitoringArticolElem.fromJson(Map<String, dynamic> parsedJson) {
    return MonitoringArticolElem(
        articolName: parsedJson['articol_name'],
        cantitate: parsedJson['cantitate'],
        valoare: parsedJson['valoare']
    );
  }

  String getValoareAsString(){
    if (valoare == null){
      return "";
    }
    return valoare.toString();
  }
  String getValoareRoundedAsString(){
    if (valoare == null){
      return "";
    }
    return double.parse(valoare.toStringAsFixed(2)).toString();
  }
}
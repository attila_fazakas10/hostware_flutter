import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:hostware_flutter/ui/centralizator_ospatar_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

ScrollController scrollControllerCentralizator;

class CentralizatorScreen extends StatefulWidget {
  String period;
  String unitate;
  CentralizatorScreen( String period_,String unitate_,{Key key}) : super(key: key) {
    period = period_;
    unitate = unitate_;
  }

  @override
  CentralizatorState createState() => CentralizatorState(this.period, this.unitate);
}

class CentralizatorState extends State<CentralizatorScreen> {
  String period;
  String unitate;

  CentralizatorIncasariListElem centralizatorIncasariListElem;
  CentralizatorState(this.period, this.unitate);

  @override
  void initState() {
    initData();

    scrollControllerCentralizator = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerCentralizator.dispose();
    super.dispose();
  }

  void initData() {
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetCentralizatorIncasariList(context, url, period, unitate).then((response) {
        setState(() {
          if (response != null) {
            centralizatorIncasariListElem = response;
          } else {
            print('initstate hivas  null ');
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {

          centralizatorIncasariListElem = new CentralizatorIncasariListElem();
        });

      }
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Centralizator incasari unitate",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin10,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Column(
        children: <Widget>[
          new Container(
            color: Theme.Colors.HWwhite,
            height: kRowHeight23,
            width: MediaQuery.of(context).size.width,
            child: new Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 5,
                        width: 25,
                        margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin10),
                        color: Theme.Colors.HWPaletteBlue2,
                      ),
                      Text(
                        "Numerar",
                        style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 5,
                        width: 25,
                        margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin10),
                        color: Theme.Colors.HWPaletteOrange1,
                      ),
                      Text(
                        "Card",
                        style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          new Container(
            color: Theme.Colors.HWwhite,
            height: kRowHeight23,
            width: MediaQuery.of(context).size.width,
            child: new Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 5,
                        width: 25,
                        margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin10),
                        color: Theme.Colors.HWPaletteGreen1,
                      ),
                      Text(
                        "Virament",
                        style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 5,
                        width: 25,
                        margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin10),
                        color: Theme.Colors.HWPaletteBlue1,
                      ),
                      Text(
                        "Tichete",
                        style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          new Container(
            color: Theme.Colors.HWwhite,
            height: kRowHeight23,
            width: MediaQuery.of(context).size.width,
            child: new Row(
              children: <Widget>[
                Expanded(
                  flex: 1,

                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 5,
                        width: 25,
                        margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin10),
                        color: Theme.Colors.HWPaletteOrange2,
                      ),
                      Text(
                        "Altele",
                        style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                    child: Container()
                ),

              ],
            ),
          )
        ],
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      centralizatorIncasariListElem!=null?
      Expanded(
          flex: 1,
          child: Container(
            color: Theme.Colors.HWwhite,
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerCentralizator,
                    itemExtent: kRowHeight80,
                    itemCount: (centralizatorIncasariListElem!=null && centralizatorIncasariListElem.incasari!=null)?centralizatorIncasariListElem.incasari.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: InkWell(
                                onTap: () => _onTapItem(context, centralizatorIncasariListElem.incasari[index]),
                                child: Column(
                                  children: <Widget>[

                                  Container(
                                  height: kRowHeight40,
                                  width: MediaQuery.of(context).size.width,
                                  color: Theme.Colors.HWwhite,
                                  child: Stack(children: <Widget>[

                                    Center(
                                      child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                        Expanded(
                                          flex: 6,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                            child: Text(centralizatorIncasariListElem.incasari[index].ospatarName, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize24)),
                                          ),
                                        ),
                                        Expanded(
                                            flex: 6,
                                            child: Align(
                                                alignment: Alignment.centerRight,
                                                child: Padding(
                                                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                                  child:
                                                      Text(NumberFormat.currency(locale: 'eu', symbol: 'lei').format(centralizatorIncasariListElem.incasari[index].totalIncasari), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize24)),
                                                ))),
                                        new Align(
                                            alignment: Alignment.centerRight,
                                            child: Image.asset(
                                              "assets/img/right_arrow.png",
                                              width: 20.0,
                                              height: 20.0,
                                            )),
                                      ]),
                                    )
                                  ]),
                                ),
                                    Container(
                                      padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: kMargin4, right: kMargin4),
                                      height: kRowHeight40,
                                      width: MediaQuery.of(context).size.width,
                                      color: Theme.Colors.HWwhite,
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            height: kRowHeight40,
                                            color: Theme.Colors.HWPaletteBlue2,
                                            width: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalNumerar,centralizatorIncasariListElem.incasari[index].totalIncasari),
                                              child: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalNumerar,centralizatorIncasariListElem.incasari[index].totalIncasari)>MediaQuery.of(context).size.width/5? Align(
                                              alignment: Alignment.center,
                                              child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariListElem.incasari[index].totalNumerar), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),)
                                                  :Container()
                                          ),
                                          Container(
                                              height: kRowHeight40,
                                              color: Theme.Colors.HWPaletteOrange1,
                                              width: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalCard, centralizatorIncasariListElem.incasari[index].totalIncasari),
                                              child: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalCard,centralizatorIncasariListElem.incasari[index].totalIncasari)>MediaQuery.of(context).size.width/5? Align(
                                                alignment: Alignment.center,
                                                child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariListElem.incasari[index].totalCard), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),):
                                                  Container()
                                          ),
                                          Container(
                                              height: kRowHeight40,
                                              color: Theme.Colors.HWPaletteGreen1,
                                              width: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalVirament, centralizatorIncasariListElem.incasari[index].totalIncasari),
                                              child: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalVirament,centralizatorIncasariListElem.incasari[index].totalIncasari)>MediaQuery.of(context).size.width/5? Align(
                                                alignment: Alignment.center,
                                                child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariListElem.incasari[index].totalVirament), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),)
                                                  :Container()
                                          ),
                                          Container(
                                              height: kRowHeight40,
                                              color: Theme.Colors.HWPaletteBlue1,
                                              width: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalTichete, centralizatorIncasariListElem.incasari[index].totalIncasari),
                                              child: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalTichete,centralizatorIncasariListElem.incasari[index].totalIncasari)>MediaQuery.of(context).size.width/5? Align(
                                                alignment: Alignment.center,
                                                child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariListElem.incasari[index].totalTichete), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),)
                                                  :Container()
                                          ),
                                          Container(
                                              height: kRowHeight40,
                                              color: Theme.Colors.HWPaletteOrange2,
                                              width: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalAltele,centralizatorIncasariListElem.incasari[index].totalIncasari),
                                              child: _calculateWidthPercentage(centralizatorIncasariListElem.incasari[index].totalAltele,centralizatorIncasariListElem.incasari[index].totalIncasari)>MediaQuery.of(context).size.width/5?
                                              Align(
                                                alignment: Alignment.center,
                                                child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariListElem.incasari[index].totalAltele), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),)
                                                  : Container()
                                          )
                                        ],
                                      )
                                    )
                                  ]))),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
          :Container(
          margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWwhite,
            strokeWidth: 1,
          ))
    ])));
  }

  void _onTapItem(BuildContext context, CentralizatorIncasariElem centralizatorIncasariElem) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              CentralizatorOspatarScreen(centralizatorIncasariElem)),
    );
  }
  double _calculateWidthPercentage(double value, double total) {

    return ((MediaQuery.of(context).size.width-(kMargin4*2))*value)/(total);

  }


  Future<CentralizatorIncasariListElem> makeGetCentralizatorIncasariList(BuildContext context, String basicUrl, String period, String unitate) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/CentralizatorOspatar"+"?period="+period+"&unitate="+unitate;
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' makeGetCentralizatorIncasariList response elot t '+url.toString());
      print('makeGetCentralizatorIncasariList  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('makeGetCentralizatorIncasariList  response elott response tostring>'+response.toString());
        CentralizatorIncasariListElem centralizatorIncasariListElem = new CentralizatorIncasariListElem();
        final responseJson = json.decode(response.body);
        centralizatorIncasariListElem = new CentralizatorIncasariListElem.fromJson(responseJson);

        return centralizatorIncasariListElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }


    return null;
  }
}

class MyDataCentralizator {
  String text;
  double total;
  List<PaymentMode> payments;

  MyDataCentralizator({this.text, this.total, this.payments});
}

class PaymentMode {
  String name;
  double value;

  PaymentMode({this.name, this.value});
}

class CentralizatorIncasariListElem {
  final List<CentralizatorIncasariElem> incasari;

  CentralizatorIncasariListElem({
    this.incasari,
  });

  factory CentralizatorIncasariListElem.fromJson(List<dynamic> parsedJson) {

    List<CentralizatorIncasariElem> incasari = new List<CentralizatorIncasariElem>();
    incasari = parsedJson.map((i)=>CentralizatorIncasariElem.fromJson(i)).toList();

    return new CentralizatorIncasariListElem(
        incasari: incasari
    );
  }
}


class CentralizatorIncasariElem{
  final int ospatarId;
  final String ospatarName;
  final double totalIncasari;
  final double totalNumerar;
  final double totalCard;
  final double totalTichete;
  final double totalVirament;
  final double totalAltele;
  final List<IncasariDetailElem> incasariDetails;

  CentralizatorIncasariElem({
    this.ospatarId,
    this.ospatarName,
    this.totalIncasari,
    this.totalNumerar,
    this.totalCard,
    this.totalTichete,
    this.totalVirament,
    this.totalAltele,
    this.incasariDetails
  }) ;

  factory CentralizatorIncasariElem.fromJson(Map<String, dynamic> parsedJson){
    var details = parsedJson['Incasari_Details']!=null?parsedJson['Incasari_Details'] as List:null;
    List<IncasariDetailElem> detailsList = details!=null? details.map((i) => IncasariDetailElem.fromJson(i)).toList():null;
    return new CentralizatorIncasariElem(
        ospatarId: parsedJson['Ospatar_Id'],
        ospatarName: parsedJson['Ospatar_Name'].toString(),
        totalIncasari: parsedJson['Total_Incasari'],
        totalNumerar: parsedJson['Total_Numerar'],
        totalCard: parsedJson['Total_Card'],
        totalTichete: parsedJson['Total_Tichete'],
        totalVirament: parsedJson['Total_Virament'],
        totalAltele: parsedJson['Total_Altele'],
        incasariDetails: detailsList
    );
  }

}


class IncasariDetailElem{
  final String date;
  final double total;
  final double numerar;
  final double card;
  final double tichete;
  final double virament;
  final double altele;

  IncasariDetailElem({
    this.date,
    this.total,
    this.numerar,
    this.card,
    this.tichete,
    this.virament,
    this.altele
  }) ;

  factory IncasariDetailElem.fromJson(Map<String, dynamic> parsedJson){
    return new IncasariDetailElem(
        date: parsedJson['Date'],
        total: parsedJson['Total'],
        numerar: parsedJson['Numerar'],
        card: parsedJson['Card'],
        tichete: parsedJson['Tichete'],
        virament: parsedJson['Virament'],
        altele: parsedJson['Altele']
    );
  }


}
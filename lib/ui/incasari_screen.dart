import 'dart:ffi';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:hostware_flutter/ui/stornari_screen.dart';
import 'package:hostware_flutter/ui/valorificare_screen.dart';
import 'package:hostware_flutter/ui/incasari_detail_screen.dart';
import 'package:hostware_flutter/ui/centralizator_screen.dart';
import 'package:hostware_flutter/ui/reducere_tichete_screen.dart';
import 'package:hostware_flutter/ui/rulaj_mese_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'package:hostware_flutter/ui/settings_screen.dart';
import 'package:hostware_flutter/ui/dropdownbutton_json_api.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

ScrollController scrollControllerIncasari;

class IncasariScreen extends StatefulWidget {
  @override
  IncasariState createState() => IncasariState();
}

class IncasariState extends State<IncasariScreen> {
  String selectedUnitate;

  String rulajBrutValue;
  String majorariValue;
  String reducereTicheteValue;
  String incasariValue;
  String noteDePlataValue;
  String oaspetiValue;
  List<GenericElem> reducereDetailsList;
  List<GenericElem> moduriDePlataList;
  List<GenericElem> stornariList;
  JsonApiDropdown unitateDropDown;
  int spy =0;

  UnitateElem _currentUnitate;

  @override
  void initState() {
    print('INCASARI SCREEN INITSTATE');
    scrollControllerIncasari = ScrollController(initialScrollOffset: 0);
//    unitateDropDown = JsonApiDropdown(selectedUnitateElem,this.callbackDropdowns, this.callbackMakeNulls, this.callbackErr);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    initData();
    super.initState();
  }


  void initData(){
    SharedPreferencesHelper.getBasicURL().then((url) {
      print("===== ERROR selectedUnitateElem 1: ${selectedUnitateElem.toString()}");


      if (selectedUnitateElem!=null && spinnerItemsUnitati!=null && spinnerItemsUnitati.length>0){
        print("===== ERROR selectedUnitateElem 1: ${selectedUnitateElem.UZEKOD.toString()}");
        setState(() {
          _currentUnitate = selectedUnitateElem;
          });
        this.callbackDropdowns(selectedUnitateElem);

      } else {
        _fetchUnitati(url).then((val) =>
            setState(() {
              spinnerItemsUnitati = val;
              if (spinnerItemsUnitati!=null && spinnerItemsUnitati.length>0) {
                if (_currentUnitate == null) {
                  _currentUnitate = spinnerItemsUnitati[0];

                } else {
                  bool kem = false;
                  for (UnitateElem unitateElem in spinnerItemsUnitati) {
                    if (unitateElem.UZEKOD == _currentUnitate.UZEKOD) {
                      _currentUnitate = unitateElem;

                      kem = true;
                    }
                  }
                  if (!kem) {
                    _currentUnitate = spinnerItemsUnitati[0];

                  }
                }
                this.callbackDropdowns(_currentUnitate);
              } else if (spinnerItemsUnitati==null){
                spinnerItemsUnitati = new List<UnitateElem>();
              }
            })).catchError((e) {
          print("===== ERROR: ${e.toString()}");
          this.callbackErr(e.toString());
        });
      }

    });
  }

  Future<List<UnitateElem>> _fetchUnitati(String basicUrl) async {

    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/Unitati";
      print('111111 HW Unitati elott ' + url);
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print('111111 HW Unitati ut ' + url);
      if (response.statusCode == 200) {
        print('200 HW Unitati ' );
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<UnitateElem> listOfUnitati = items.map<UnitateElem>((json) {
          return UnitateElem.fromJson(json);
        }).toList();

        return listOfUnitati;
      } else {
        print('333333 HW Unitati ' + response.statusCode.toString());
        throw Exception(response.statusCode.toString()+'<statuscode');
      }
    } on TimeoutException catch (_) {
      print('Timeout??? ');
      throw Exception('Timeout');
      // A timeout occurred.
    } on Exception catch (_) {
      print('Exception?????'+_.toString());
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      print('SEVERHIBAAA');
      print('SEVERHIBAAA:'+exception.toString());
      throw Exception(exception.toString());
    }


    return null;


  }

  @override
  void dispose() {
    scrollControllerIncasari.dispose();
    super.dispose();
  }

  // Init state logic
  Future initStateCustome() async {}

  _scrollToTop() {
    scrollControllerIncasari.animateTo(scrollControllerIncasari.position.minScrollExtent, duration: Duration(milliseconds: 100), curve: Curves.easeIn);
//    setState(() => _isOnTop = true);
  }

  void executeAfterWholeBuildProcess(BuildContext context) {
    // executes after build is done
    _scrollToTop();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Acasa/Incasari",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.settings, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SettingsScreen()),
                      ).then((value) {
                        setState(() {
                          if (selectedUnitateElem!=null) {
                            callbackMakeNulls();
                            callbackDropdowns(selectedUnitateElem);
                          }
                        });
                      }),
                    )),
              ),
            ],
          ),
        ),
      ),
      Expanded(
          flex: 1,
          child: Container(
              width: double.infinity,
              child: SingleChildScrollView(
                controller: scrollControllerIncasari,
                child: Column(
                  children: <Widget>[
                    Container(
                      height: kBarHeight,
                      child: Padding(
                          padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                          child: Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                            Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                  child: DropDownPeriod(this.callbackDropdowns, this.callbackMakeNulls),
                                )),
                            Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                  child: spinnerItemsUnitati!=null && spinnerItemsUnitati.length>0? new JsonApiDropdown(selectedUnitateElem, spinnerItemsUnitati,this.callbackDropdowns, this.callbackMakeNulls):
                                  spinnerItemsUnitati==null?Align(
                                  alignment: Alignment.center,
                                    child:Container(
                                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                      width: kProgressSize,
                                      height: kProgressSize,
                                      child: CircularProgressIndicator(
                                        backgroundColor: Theme.Colors.HWwhite,
                                        strokeWidth: 1,
                                      )),
                                ):Container()))
                          ])),
                    ),
                    Container(
                      height: kRowHeight68,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Container(
                          height: kRowHeight68,
                          color: Theme.Colors.HWrowGrey,
                          child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                              Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.bottomCenter,
                              child: Text('Rulaj brut', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                              )),
                          Expanded(
                              flex: 1,
                              child: Align(
                                  alignment: Alignment.topCenter,
                                  child: rulajBrutValue!=null? Text(rulajBrutValue, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24)):
                                  Container(
                                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                      width: kProgressSize,
                                      height: kProgressSize,
                                      child: CircularProgressIndicator(
                                        backgroundColor: Theme.Colors.HWwhite,
                                        strokeWidth: 1,
                                      ))),
                          )]),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Row(children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 1.0),
                              height: kRowHeight68,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWrowGrey,
                              child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Text('Majorari', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.topCenter,
                                      child: majorariValue != null
                                          ? Text(majorariValue, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14))
                                          : Container(
                                              margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                              width: kProgressSize,
                                              height: kProgressSize,
                                              child: CircularProgressIndicator(
                                                backgroundColor: Theme.Colors.HWwhite,
                                                strokeWidth: 1,
                                              )),
                                    )),
                              ]),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: new InkWell(
                                onTap: () => _onTapReducereTichete(context, reducereDetailsList),
                                child: Container(
                                    height: kRowHeight68,
                                    width: MediaQuery.of(context).size.width,
                                    color: Theme.Colors.HWrowGrey,
                                    padding: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                                    child: Stack(
                                      children: <Widget>[
                                        new Align(
                                            alignment: Alignment.bottomRight,
                                            child: Image.asset(
                                              "assets/img/polygon.png",
                                              width: 18.0,
                                              height: 18.0,
//                                      fit: BoxFit.cover,
                                            )),
                                        Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                          Expanded(
                                              flex: 1,
                                              child: Align(
                                                alignment: Alignment.bottomCenter,
                                                child: Text('Reducere/tichete', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                              )),
                                          Expanded(
                                            flex: 1,
                                            child: Align(
                                                alignment: Alignment.topCenter,
                                                child: reducereTicheteValue != null
                                                    ? (Text(reducereTicheteValue, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14)))
                                                    : Container(
                                                        margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                                        width: kProgressSize,
                                                        height: kProgressSize,
                                                        child: CircularProgressIndicator(
                                                          backgroundColor: Theme.Colors.HWwhite,
                                                          strokeWidth: 1,
                                                        ))),
                                          )
                                        ]),
                                      ],
                                    ))),
                          ),
                        ])),
                    Padding(
                        padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: new InkWell(
                            onTap: () => _onTapIncasari(context, moduriDePlataList),

                            child: Container(
                                height: kRowHeight68,
                                width: MediaQuery.of(context).size.width,
                                color: Theme.Colors.HWrowGrey,
                                padding: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                                child: Stack(
                                  children: <Widget>[
                                    new Align(
                                        alignment: Alignment.bottomRight,
                                        child: Image.asset(
                                          "assets/img/polygon.png",
                                          width: 18.0,
                                          height: 18.0,
//                                      fit: BoxFit.cover,
                                        )),
                                    Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                      Expanded(
                                          flex: 1,
                                          child:Align(
                                            alignment: Alignment.bottomCenter,
                                        child: Text('Incasari', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                      )),
                                      Expanded(
                                          flex: 1,
                                        child:Align(
                                            alignment: Alignment.topCenter,child: incasariValue!=null?Text(incasariValue, style: TextStyle(fontWeight: FontWeight.bold, color: Theme.Colors.HWyellow, fontSize: kFontSize24)):
                                        Container(
                                            margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                            width: kProgressSize,
                                            height: kProgressSize,
                                            child: CircularProgressIndicator(
                                              backgroundColor: Theme.Colors.HWwhite,
                                              strokeWidth: 1,
                                            ))),
                                      )]),
                                  ],
                                )))),
                    Padding(
                        padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Row(children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 1.0),
                              height: kRowHeight68,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWrowGrey,
                              child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                  Expanded(
                                  flex: 1,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                  child: Text('Note de plata', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                  )),
                              Expanded(
                                  flex: 1,
                                  child: Align(
                                      alignment: Alignment.topCenter,child: noteDePlataValue!=null? Text(noteDePlataValue, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24)):
                                  Container(
                                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                      width: kProgressSize,
                                      height: kProgressSize,
                                      child: CircularProgressIndicator(
                                        backgroundColor: Theme.Colors.HWlightGrey,
                                        strokeWidth: 1,
                                      ))),
                              )]),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: kRowHeight68,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWrowGrey,
                              child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                  Expanded(
                                  flex: 1,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                  child: Text('Oaspeti', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                  )),
                              Expanded(
                                  flex: 1,
                                  child: Align(
                                      alignment: Alignment.topCenter,child: oaspetiValue!=null?Text(oaspetiValue, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24)):
                                  Container(
                                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                                      width: kProgressSize,
                                      height: kProgressSize,
                                      child: CircularProgressIndicator(
                                        backgroundColor: Theme.Colors.HWlightGrey,
                                        strokeWidth: 1,
                                      ))),
                              )]),
                            ),
                          ),
                        ])),
                    Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width,
                          color: Theme.Colors.HWrowGrey,
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: new InkWell(
                            onTap: () => _onTapStornari(context, stornariList),

                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWwhite,
                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                Text('Stornari', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                      "assets/img/right_arrow.png",
                                      width: 24.0,
                                      height: 24.0,
//                                      fit: BoxFit.cover,
                                    )),
                              ]),
                            ))),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width,
                          color: Theme.Colors.HWrowGrey,
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: new InkWell(
                            onTap: () => _onTapValorificare(context, periodValue),

                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWwhite,
                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                Text('Valorificare articole', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                      "assets/img/right_arrow.png",
                                      width: 24.0,
                                      height: 24.0,
//                                      fit: BoxFit.cover,
                                    )),
                              ]),
                            ))),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width,
                          color: Theme.Colors.HWrowGrey,
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: new InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => CentralizatorScreen(periodValue, selectedUnitate)),
                              );
                            },
                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWwhite,
                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                Text('Centralizator incasari unitate', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                      "assets/img/right_arrow.png",
                                      width: 24.0,
                                      height: 24.0,
//                                      fit: BoxFit.cover,
                                    )),
                              ]),
                            ))),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width,
                          color: Theme.Colors.HWrowGrey,
                        )),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                        child: new InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => RulajMeseScreen(periodValue, selectedUnitate)),
                              );
                            },
                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWwhite,
                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                Text('Rulaj mese', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                      "assets/img/right_arrow.png",
                                      width: 24.0,
                                      height: 24.0,
//                                      fit: BoxFit.cover,
                                    )),
                              ]),
                            ))),
                    Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin4, right: kMargin4),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width,
                          color: Theme.Colors.HWrowGrey,
                        )),
                  ],
                ),
              )))
    ]));
  }

  void callbackDropdowns(UnitateElem unitateElem) {

    SharedPreferencesHelper.getBasicURL().then((url) {
      setState(() {
        if (unitateElem!=null && unitateElem.UZEKOD!=null) {
          selectedUnitate = unitateElem.UZEKOD.toString();
          print("===== ERROR selectedUnitate: ${selectedUnitate.toString()}");

          selectedUnitateElem = unitateElem;
        }
        makeGetCentralizator(context, url, dropdownValue == null ? '30' : dropdownValue.contains('30') ? '30' : dropdownValue.contains('7') ? '7' : dropdownValue.contains('anterioara') ? '2' : '1', selectedUnitate)
            .then((response) {
          setState(() {
            print('CENT initstate response FO');

            if (response != null) {
              print('CENT initstate hivas nem null ');
              rulajBrutValue = response.rulajBrut != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.rulajBrut) : "-";
              majorariValue = response.majorari != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.majorari) : "-";
              reducereTicheteValue = response.reduceri != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.reduceri) : "-";
              incasariValue = response.incasari != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.incasari) : "-";
              noteDePlataValue = response.nrNote != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.nrNote) : "-";
              oaspetiValue = response.nrOaspeti != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.nrOaspeti) : "-";
              reducereDetailsList = response.reducereDetails;
              moduriDePlataList = response.moduriDePlata;
              stornariList = response.stornari;
            } else {
              print('initstate hivas  null ');
              rulajBrutValue = "-";
              majorariValue = "-";
              reducereTicheteValue = "-";
              incasariValue = "-";
              noteDePlataValue = "-";
              oaspetiValue = "-";
              reducereDetailsList = null;
              moduriDePlataList = null;
              stornariList = new List<GenericElem>();
              this.callbackErr("server");
            }
          });
        }).catchError((e) {
          print("===== ERROR: ${e.toString()}");
          this.callbackErr(e.toString());
        });
      });
    });
  }
  void callbackErr(String err) {
    if (err.contains("general_error") ){
      print(' general_error if 1 ');
      _showErrorDialog();
    } else {
      print(' general_error if 2 ');
      callbackMakeNulls();
      _displayErrorScreen(context, err);
    }


  }

  void callbackMakeNulls() {
    setState(() {
      rulajBrutValue = null;
      majorariValue = null;
      reducereTicheteValue = null;
      incasariValue = null;
      noteDePlataValue = null;
      oaspetiValue = null;
      reducereDetailsList =null;
      moduriDePlataList = null;
      stornariList = null;
    });

  }

  Future<CentralizatorElem> makeGetCentralizator(BuildContext context, String basicUrl, String period, String unitate) async {
    try {
      var headers = {'Accept': 'application/json'};
//      period = "11510";
      var url =basicUrl+ "/Centralizator" + "?period=" + period + "&unitate=" + unitate;
      var response = await http.get(url, headers: headers).timeout(const Duration(seconds: 30));
      print(' centr response elot t ' + url.toString());
      print('centr  response elott 2 ' + response.statusCode.toString());
      if (response.statusCode == 200) {
//        print('centr  response elott response tostring>' + response.toString());
        CentralizatorElem centralizatorElem = new CentralizatorElem();
        final responseJson = json.decode(response.body);
        print('centr  responseJson');
        centralizatorElem = new CentralizatorElem.fromJson(responseJson);
        print('centr  responseJson2');
//        print('centralizatorElem.rulajBrut>:' + centralizatorElem.rulajBrut.toString());

        return centralizatorElem;
      } else {
        print('throw general_error');
        throw Exception('general_error');
      }
    }  on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }

    return null;
  }

  _onTapIncasari(BuildContext context, List<GenericElem> moduriDePlataList) {
    if (moduriDePlataList!=null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => IncasariDetailScreen(moduriDePlataList)),
      );
    }
  }
  void _showErrorDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Eroare"),
          content: new Text("Problemă de comunicare"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
//                if (spinnerItemsUnitati!=null && spinnerItemsUnitati.length>1) {
//                    _currentUnitate = spinnerItemsUnitati[1];
//
//                  this.callbackDropdowns(_currentUnitate);
//                }
              },
            ),
          ],
        );
      },
    );
  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {
          rulajBrutValue = "-";
          majorariValue = "-";
          reducereTicheteValue = "-";
          incasariValue = "-";
          noteDePlataValue = "-";
          oaspetiValue = "-";
          reducereDetailsList = null;
          moduriDePlataList = null;
          stornariList = new List<GenericElem>();
          spinnerItemsUnitati = new List<UnitateElem>();
        });

      }
    });
  }
  _onTapReducereTichete(BuildContext context, List<GenericElem> reducereDetailsList) {
    if (reducereDetailsList!=null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ReducereTicheteScreen(reducereDetailsList)),
      );
    }
  }

  _onTapStornari(BuildContext context, List<GenericElem> stornariList) {
    if (stornariList!=null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => StornariScreen(stornariList)),
      );
    }
  }
  _onTapValorificare(BuildContext context, String period) {
    if (period!=null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ValorificareScreen(period)),
      );
    }
  }
}

String dropdownValue = 'Ultimele 30 de zile';
String periodValue = '30';
UnitateElem selectedUnitateElem;
List<UnitateElem> spinnerItemsUnitati;
class DropDownPeriod extends StatefulWidget {
//  Function callback2;

  final void Function(UnitateElem) callback;
  final void Function() callbackMakeNulls;
  DropDownPeriod(this.callback, this.callbackMakeNulls);

  @override
  DropDownWidget createState() => DropDownWidget(callback, callbackMakeNulls);
}

List<String> spinnerItems = ['Ultimele 30 de zile', 'Ultimele 7 zile', 'Ziua anterioara', 'Ziua curenta'];

class DropDownWidget extends State {
  final void Function(UnitateElem) callback;
  final void Function() callbackMakeNulls;
  DropDownWidget(this.callback, this.callbackMakeNulls);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(children: <Widget>[
          DropdownButton<String>(
            value: dropdownValue,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 19,
            elevation: 4,
            style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14),
            onChanged: (String data) {
              setState(() {
                dropdownValue = data;
                periodValue = dropdownValue == null ? '30' : dropdownValue.contains('30') ? '30' : dropdownValue.contains('7') ? '7' : dropdownValue.contains('anterioara') ? '2' : '1';
                this.callbackMakeNulls();
                this.callback(null);


              });
            },
            items: spinnerItems.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),

//          Text('Selected Item = ' + '$dropdownValue',
//              style: TextStyle
//                (fontSize: 12,
//                  color: Colors.black)),
        ]),
      ),
    );
  }
}

class IncasariElem {
  final String title;
  final double value;

  IncasariElem({this.title, this.value});

  factory IncasariElem.fromJson(Map<String, dynamic> parsedJson) {
    return IncasariElem(title: parsedJson['Title'], value: parsedJson['Value']);
  }

  String getValueAsString() {
    if (value == null) {
      return "";
    }
    return value.toString();
  }
}

class GenericElem {
  final String title;
  final double value;

  GenericElem({this.title, this.value});

  factory GenericElem.fromJson(Map<String, dynamic> parsedJson) {
    return GenericElem(title: parsedJson['Title'], value: parsedJson['Value']);
  }

  String getValueAsString() {
    if (value == null) {
      return "";
    }
    return value.toString();
  }

  String getValueRoundedAsString() {
    if (value == null) {
      return "";
    }
    return double.parse(value.toStringAsFixed(2)).toString();
  }
}

class CentralizatorElem {
  final double rulajBrut;
  final double majorari;
  final double reduceri;
  final double incasari;
  final double nrNote;
  final double nrOaspeti;
  final List<GenericElem> reducereDetails;
  final List<GenericElem> moduriDePlata;
  final List<GenericElem> stornari;

  CentralizatorElem({this.rulajBrut, this.majorari, this.reduceri, this.incasari, this.nrNote, this.nrOaspeti, this.reducereDetails, this.moduriDePlata, this.stornari});

  factory CentralizatorElem.fromJson(Map<String, dynamic> parsedJson) {
    var reducereDetails = parsedJson['ReducereDetails']!=null?parsedJson['ReducereDetails'] as List:null;
    var moduriDePlata = parsedJson['Moduri_De_Plata']!=null?  parsedJson['Moduri_De_Plata'] as List:null;
    var stornari =parsedJson['Stornari']!=null?parsedJson['Stornari'] as List:null;
    List<GenericElem> reducereDetailsList = reducereDetails!=null?reducereDetails.map((i) => GenericElem.fromJson(i)).toList():null;
    List<GenericElem> moduriDePlataList = moduriDePlata!=null?moduriDePlata.map((i) => GenericElem.fromJson(i)).toList():null;
    List<GenericElem> stornariList = stornari!=null?stornari.map((i) => GenericElem.fromJson(i)).toList():null;
    return CentralizatorElem(
        rulajBrut: parsedJson['Rulaj_Brut'],
        majorari: parsedJson['Majorari'],
        reduceri: parsedJson['Reduceri'],
        incasari: parsedJson['Incasari'],
        nrNote: parsedJson['Nr_Note'],
        nrOaspeti: parsedJson['Nr_oaspeti'],
        reducereDetails: reducereDetailsList,
        moduriDePlata: moduriDePlataList,
        stornari: stornariList);
  }
}

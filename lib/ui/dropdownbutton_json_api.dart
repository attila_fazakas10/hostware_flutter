import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/incasari_screen.dart';
import 'dart:convert';
import 'dart:async';
import 'package:hostware_flutter/utils/shared_pref.dart';

import 'package:http/http.dart' as http;
class JsonApiDropdown extends StatefulWidget {
  UnitateElem preselectedUnitate;
  List<UnitateElem> spinnerItems;
  final void Function(UnitateElem) callback;
  final void Function() callbackMakeNulls;

  JsonApiDropdown(this.preselectedUnitate, this.spinnerItems,this.callback, this.callbackMakeNulls);


  @override
  JsonApiDropdownState createState() {
    return new JsonApiDropdownState(preselectedUnitate,spinnerItems, callback, callbackMakeNulls);
  }

}

class JsonApiDropdownState extends State {
  UnitateElem _currentUnitate;
  List<UnitateElem> spinnerItems;

  final void Function(UnitateElem) callback;
  final void Function() callbackMakeNulls;
  JsonApiDropdownState(this._currentUnitate, this.spinnerItems, this.callback, this.callbackMakeNulls);


  @override
  void initState() {
//    initData();
    super.initState();
  }
/*

  void initData(){
    SharedPreferencesHelper.getBasicURL().then((url) {
      _fetchUnitati(url).then((val) =>
          setState(() {
            spinnerItems = val;
            if (spinnerItems!=null && spinnerItems.length>0) {
              if (_currentUnitate == null) {
                _currentUnitate = spinnerItems[0];
              } else {
                bool kem = false;
                for (UnitateElem unitateElem in spinnerItems) {
                  if (unitateElem.UZEKOD == _currentUnitate.UZEKOD) {
                    _currentUnitate = unitateElem;
                    kem = true;
                  }
                }
                if (!kem) {
                  _currentUnitate = spinnerItems[0];
                }
              }
              this.callback(_currentUnitate);
            }
          })).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  Future<List<UnitateElem>> _fetchUnitati(String basicUrl) async {

    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/Unitati";
      print('111111 HW Unitati elott ' + url);
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print('111111 HW Unitati ut ' + url);
      if (response.statusCode == 200) {
        print('200 HW Unitati ' );
        final items = json.decode(response.body).cast<Map<String, dynamic>>();
        List<UnitateElem> listOfUnitati = items.map<UnitateElem>((json) {
          return UnitateElem.fromJson(json);
        }).toList();
        print('222222 HW Unitati ' + (_currentUnitate==null?"nulll":_currentUnitate.UZENEV));

//        if (_currentUnitate==null) {
//          setState(() {
//            _currentUnitate = listOfUnitati[0];
//          });
//
//        }
        return listOfUnitati;
      } else {
        print('333333 HW Unitati ' + response.statusCode.toString());
        throw Exception(response.statusCode.toString()+'<statuscode');
      }
    } on TimeoutException catch (_) {
      print('Timeout??? ');
      throw Exception('Timeout');
      // A timeout occurred.
    } on Exception catch (_) {
      print('Exception?????'+_.toString());
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      print('SEVERHIBAAA');
      print('SEVERHIBAAA:'+exception.toString());
      throw Exception(exception.toString());
    }


    return null;


  }
*/

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        body: Center(
          child:  spinnerItems!=null?Column(children: <Widget>[

            DropdownButton<UnitateElem>(
              value: _currentUnitate,
              icon: Icon(Icons.arrow_drop_down),
              iconSize: 19,
              elevation: 4,
              style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14),
              onChanged: (UnitateElem data) {
                setState(() {
                  _currentUnitate = data;
                  this.callbackMakeNulls();
                  this.callback(_currentUnitate);
                });
              },
              items: spinnerItems!=null?spinnerItems.map<DropdownMenuItem<UnitateElem>>((UnitateElem value) {
                return DropdownMenuItem<UnitateElem>(
                  value: value,
                  child: Text(value.UZENEV),
                );
              }).toList():null,
            ),

//          Text('Selected Item = ' + '$dropdownValue',
//              style: TextStyle
//                (fontSize: 12,
//                  color: Colors.black)),
          ]):Container(
        margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWlightGrey,
            strokeWidth: 1,
          )),
        ),
      );
  }
}
/*

class Users {
  int id;
  String name;
  String username;
  String email;

  Users({
    this.id,
    this.name,
    this.username,
    this.email,
  });

  factory Users.fromJson(Map<String, dynamic> json) {
    return Users(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      username: json['username'],
    );
  }
}*/


class UnitateElem {
  final int UZEKOD;
  final int TELNUM;
  final String UZENEV;

  UnitateElem(
      {this.UZEKOD,
        this.TELNUM,
        this.UZENEV});

  factory UnitateElem.fromJson(Map<String, dynamic> parsedJson) {
    return UnitateElem(
      UZEKOD: parsedJson['UZEKOD'],
      TELNUM: parsedJson['TELNUM'],
      UZENEV: parsedJson['UZENEV']
    );
  }
}
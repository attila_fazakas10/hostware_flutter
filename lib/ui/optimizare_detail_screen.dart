import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/optimizare_screen.dart';
import 'package:flutter/services.dart';

ScrollController scrollControllerOptimizareDetail;

class OptimizareDetailScreen extends StatefulWidget {
  MyDataOptimizare data;

  OptimizareDetailScreen(MyDataOptimizare data_, {Key key}) : super(key: key) {
    data = data_;
  }

  @override
  OptimizareDetailState createState() => OptimizareDetailState(this.data);
}

class OptimizareDetailState extends State<OptimizareDetailScreen> {
  List<MyDataOptimizareDetail> myDatas;
  MyDataOptimizare data;

  OptimizareDetailState(this.data);

  @override
  void initState() {
    initData();

    scrollControllerOptimizareDetail = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerOptimizareDetail.dispose();
    super.dispose();
  }

  void initData() {
    setState(() {
      myDatas = new List();
      myDatas.add(new MyDataOptimizareDetail(text: "Vanzare", percentageValue: "5.67%", pretMediu: 999.00, cantitate: 80, valoareNet: 730.00));
      myDatas.add(new MyDataOptimizareDetail(text: "Castig", percentageValue: "0.89%", pretMediu: 1.00, cantitate: 80, valoareNet: 80.00));
      myDatas.add(new MyDataOptimizareDetail(text: "Materii prime", percentageValue: "", pretMediu: 8.00, cantitate: 980, valoareNet: 18650.00));
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Optimizare profit",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      new Container(
        decoration: BoxDecoration(
            color: Theme.Colors.HWwhite,
            borderRadius: BorderRadius.all(Radius.circular(0.0)),
            border: Border.all(
              color: (data.category == "Star")
                  ? Theme.Colors.HWPaletteGreen1
                  : (data.category == "Puzzle") ? Theme.Colors.HWPaletteBlue2 : (data.category == "Dog") ? Theme.Colors.HWPaletteOrange1 : Theme.Colors.HWPaletteBlue1,
              width: 3,
            )),
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: kMargin30),
              child: Container(
                child: Text(
                  data.text,
                  style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                ),
              ),
            ),
          ],
        ),
      ),
      Container(
          padding: EdgeInsets.only(top: kMargin10, bottom: 0.0, left: kMargin4, right: kMargin4),

          alignment: Alignment.centerRight,
          child: Container(

            color: (data.category == "Star")
                ? Theme.Colors.HWPaletteGreen1
                : (data.category == "Puzzle") ? Theme.Colors.HWPaletteBlue2 : (data.category == "Dog") ? Theme.Colors.HWPaletteOrange1 : Theme.Colors.HWPaletteBlue1,
            padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: kMargin10, right: kMargin10),
            child: Text(data.category, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14)),
          )),
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerOptimizareDetail,
                    itemCount: myDatas.length,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: kMargin4),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              child: Column(children: <Widget>[
                                Container(
                                    height: kRowHeight60,
                                    padding: EdgeInsets.only(top: 0.0, bottom: kMargin10, left: 0.0, right: 0.0),
                                  alignment: Alignment.bottomCenter,

                                  child:  new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                    Text(myDatas[index].text, style: TextStyle(fontWeight: FontWeight.normal, color:myDatas[index].percentageValue==""?Theme.Colors.HWBlackTextColor:Theme.Colors.HWred, fontSize: kFontSize24)),
                                    Text(myDatas[index].percentageValue, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWred, fontSize: kFontSize24)),
                                  ])
                                ),

                                Container(
                                    height: kBarHeight,
                                    width: MediaQuery.of(context).size.width,
                                    color: Theme.Colors.HWrowGrey,
                                    padding: EdgeInsets.only(top: 0.0, bottom: 1.0, left: kMargin10, right: kMargin10),
                                    child: Stack(
                                      children: <Widget>[

                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text("Pret mediu", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                                  ),
                                                  Align(
                                                      alignment: Alignment.bottomLeft,child: Text(myDatas[index].pretMediu.toStringAsFixed(2) + " lei",
                                                      style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                                                ])),
                                            Expanded(
                                                flex: 3,
                                                child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[

                                                  Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Text("Cantitate", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                                  ),
                                                  Align(
                                                      alignment: Alignment.bottomLeft,child: Text(myDatas[index].cantitate.toString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                                                ])),
                                            Expanded(
                                                flex: 6,
                                                child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                  Align(
                                                    alignment: Alignment.topRight,
                                                    child: Text("Valoare net", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                                                  ),
                                                  Align(
                                                      alignment: Alignment.bottomRight,child: Text(myDatas[index].valoareNet.toStringAsFixed(2) + " lei",
                                                      style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                                                ])),
                                          ],
                                        )
                                      ],
                                    ))

                              ]),
                            )),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
    ])));
  }
}

class MyDataOptimizareDetail {
  String text;
  String percentageValue;
  double pretMediu;
  int cantitate;
  double valoareNet;

  MyDataOptimizareDetail({this.text, this.percentageValue, this.pretMediu, this.cantitate, this.valoareNet});
}

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/parteneri_detail_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:http/http.dart' as http;
Timer _timer;
int _start = 3;
class ParteneriScreen extends StatefulWidget {
  @override
  ParteneriState createState() => ParteneriState();
}

class ParteneriState extends State<ParteneriScreen> {
  List<PartenerElem> parteneriList;
  bool needEmptyDialog = false;
  TextEditingController controller = new TextEditingController();
  String filter;

  @override
  void initState() {
    print('init state miatt hivodik!!!!!!!!!!!!!!');
    callbackDropdowns();
    controller.addListener(() {
      setState(() {
        filter = controller.text;
      });
    });

    initStateCustome().then((_) {
       setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    if (_timer!=null) {
      _timer.cancel();
    }
    super.dispose();
  }

  // Init state logic
  Future initStateCustome() async {}

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
            children: <Widget>[
      Container(
        height: kBarHeight,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Parteneri",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      Container(
        height: kBarHeight,
        child: Padding(
            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
            child: Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                    child: DropDownPeriod(callbackDropdowns, callbackMakeNulls),
                  )),
              Expanded(
                  flex: 1,
                  child: Container())
            ])),
      ),
      Container(
          height: kRowHeight35,
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(top: 0.0, bottom: kMargin20, left: kMargin20, right: kMargin20),
          child: new TextField(
            autofocus: false,
            style: TextStyle(fontSize: kFontSize14),
            decoration: new InputDecoration(
              suffixIcon: filter==null||filter==""?Icon(Icons.search, color: Theme.Colors.HWlightGrey):IconButton(
                onPressed: () {
                  this.setState(() {
                    controller.clear();
                  });
                },
                icon: Icon(Icons.close,color: Theme.Colors.HWlightGrey),
              ),

              contentPadding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: kMargin20),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.Colors.HWlightGrey, width: 1.0),
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.Colors.HWlightGrey, width: 1.0),
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
              ),
              hintText: 'Search',
            ),
            controller: controller,
          )),

              parteneriList !=null && parteneriList.length>0?
              new Expanded(
                  child: new ListView.builder(
                    itemCount: parteneriList != null ? parteneriList.length : 0,
                    itemBuilder: (BuildContext context, int index) {
                      return filter == null || filter == "" ? buildRow(parteneriList[index]) : parteneriList[index].partnerName.toLowerCase().contains(filter.toLowerCase()) ? buildRow(parteneriList[index]) : new Container();
                    },
                  )):parteneriList ==null?
              Container(
                  margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                  width: kProgressSize,
                  height: kProgressSize,
                  child: CircularProgressIndicator(
                    backgroundColor: Theme.Colors.HWwhite,
                    strokeWidth: 1,
                  )):Container()
              ,
                  Expanded(
                    flex:parteneriList!=null && parteneriList.length==0 ?1:0,
                    child: Container(),
                  ),

                  new AnimatedContainer(
                      decoration: BoxDecoration(
                          color: Theme.Colors.HWwhite,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(
                            color: Theme.Colors.HWGreyBorderColor,
                            width: 1,
                          )),

                      curve: Curves.linearToEaseOut,
                      height: parteneriList!=null && parteneriList.length==0 && needEmptyDialog? 185:0,
                      duration: new Duration(milliseconds: 500),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          color: Theme.Colors.HWwhite,
                          height: parteneriList!=null && parteneriList.length==0 && needEmptyDialog?185:0,
                          width:  MediaQuery.of(context).size.width,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: kMargin10, bottom: 0.0, left: 0.0, right: 0.0),
                                child: new Align(

                                    alignment: Alignment.topCenter,
                                    child: Image.asset(
                                      "assets/img/alert_circle.png",
                                      width: 76.0,
                                      height: 76.0,
//                                      fit: BoxFit.cover,
                                    )),
                              ),
                              Expanded(
                                child:Container(
                                    margin: EdgeInsets.only(top: 0.0, bottom: kMargin10, left: 0.0, right: 0.0),
                                    child: new Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Text("Nu sunt date pentru\nperioada selectata!", textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWrowGrey, fontSize: kFontSize14)),
                                    )),
                              )

                            ],
                          ),
                        ),
                      )),





    ]));
  }

  Container buildRow(PartenerElem partenerElem) {
    return Container(
        padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
        child: new InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ParteneriDetailScreen(partenerElem, periodValue)),
              );
            },
            child: Container(
                height: kBarHeight,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWrowGrey,
                padding: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                child: Stack(
                  children: <Widget>[
                    new Align(
                        alignment: Alignment.bottomRight,
                        child: Image.asset(
                          "assets/img/polygon.png",
                          width: 18.0,
                          height: 18.0,
//                                      fit: BoxFit.cover,
                        )),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 6,
                          child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin4, right: kMargin4),
                              child: Text(partenerElem.partnerName, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14)),
                            ),
                          ]),
                        ),
                        Expanded(
                            flex: 3,
                            child: Container()),
                        Expanded(
                            flex: 6,
                            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text("Valoare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                              ),
                              Align(
                                  alignment: Alignment.bottomLeft,child: Text(NumberFormat.currency(locale: 'eu', symbol: 'lei').format(partenerElem.value), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                            ])),
                      ],
                    )
                  ],
                ))));
  }
  void startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      if (_start < 1) {
        _timer.cancel();
        setState(() {
          _start = 3;
          needEmptyDialog = false;
        });
      } else {
        _start = _start - 1;
      };
    });
  }
  void callbackDropdowns() {
    if (_timer!=null){
      _timer.cancel();
      _start = 3;
    }
    setState(() {
      needEmptyDialog = false;
    });
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetParteneriList(context, url, dropdownValue == null ? '30' : dropdownValue.contains('30') ? '30' : dropdownValue.contains('7') ? '7' : dropdownValue.contains('anterioara') ? '2' : '1').then((response) {
        setState(() {
          if (response != null) {

            parteneriList = response.parteneri;
            if (parteneriList==null){
              parteneriList = new List<PartenerElem>();

            }
            if (parteneriList.length == 0) {
              needEmptyDialog = true;
              startTimer();
            }
          } else {
            print("===== null: ");
            parteneriList = new List<PartenerElem>();
            needEmptyDialog = true;
            startTimer();
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          callbackDropdowns();

//        }
        });
      } else {
        setState(() {
          parteneriList = new List<PartenerElem>();
        });

      }
    });
  }
  void callbackMakeNulls() {
    setState(() {
      parteneriList = null;
    });

  }


  Future<PartenerListElem> makeGetParteneriList(BuildContext context, String basicUrl, String period) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/Partners"+"?period="+period;
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' makeGetParteneriList response elot t '+url.toString());
      print('makeGetParteneriList  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('makeGetParteneriList  response elott response tostring>'+response.toString());
        PartenerListElem partenerListElem = new PartenerListElem();
        final responseJson = json.decode(response.body);
        partenerListElem = new PartenerListElem.fromJson(responseJson);

        return partenerListElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }

    return null;
  }
}

String dropdownValue = 'Ultimele 30 de zile';
String periodValue = '30';
class DropDownPeriod extends StatefulWidget {
//  Function callback2;

  final void Function() callback;
  final void Function() callbackMakeNulls;
  DropDownPeriod(this.callback, this.callbackMakeNulls);

  @override
  DropDownWidget createState() => DropDownWidget(callback, callbackMakeNulls);
}

List<String> spinnerItems = ['Ultimele 30 de zile', 'Ultimele 7 zile', 'Ziua anterioara', 'Ziua curenta'];

class DropDownWidget extends State {
  final void Function() callback;
  final void Function() callbackMakeNulls;
  DropDownWidget(this.callback, this.callbackMakeNulls);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(children: <Widget>[
          DropdownButton<String>(
            value: dropdownValue,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 19,
            elevation: 4,
            style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14),
            onChanged: (String data) {
              setState(() {
                FocusScope.of(context).requestFocus(new FocusNode());
                dropdownValue = data;


                periodValue = dropdownValue == null ? '30' : dropdownValue.contains('30') ? '30' : dropdownValue.contains('7') ? '7' : dropdownValue.contains('anterioara') ? '2' : '1';
                print('onchange miatt hivodik');
                this.callbackMakeNulls();
                this.callback();


              });
            },
            items: spinnerItems.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),

//          Text('Selected Item = ' + '$dropdownValue',
//              style: TextStyle
//                (fontSize: 12,
//                  color: Colors.black)),
        ]),
      ),
    );
  }
}/*
class DropDown extends StatefulWidget {
  @override
  DropDownWidget createState() => DropDownWidget();
}

class DropDownWidget extends State {
  String dropdownValue = 'Ultimele 30 de zile';

  List<String> spinnerItems = ['Ziua curenta', 'Ziua anterioara', 'Ultimele 7 de zile', 'Ultimele 30 de zile'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(children: <Widget>[
          DropdownButton<String>(
            value: dropdownValue,
            icon: Icon(Icons.keyboard_arrow_down),
            iconSize: 19,
            elevation: 4,
            style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14),
            onChanged: (String data) {
              setState(() {
                dropdownValue = data;
              });
            },
            items: spinnerItems.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),

//          Text('Selected Item = ' + '$dropdownValue',
//              style: TextStyle
//                (fontSize: 12,
//                  color: Colors.black)),
        ]),
      ),
    );
  }


}*/

class PartenerListElem {
  final List<PartenerElem> parteneri;

  PartenerListElem({
    this.parteneri,
  });

  factory PartenerListElem.fromJson(List<dynamic> parsedJson) {

    List<PartenerElem> parteneri = new List<PartenerElem>();
    parteneri = parsedJson.map((i)=>PartenerElem.fromJson(i)).toList();

    return new PartenerListElem(
        parteneri: parteneri
    );
  }
}

class PartenerElem{
  final int partnerId;
  final String partnerName;
  final double value;
  final List<NoteElem> notes;
  final List<PartenerArticolElem>  articole;
  final List<ModDePlataElem>   moduriDePlata;

  PartenerElem({
    this.partnerId,
    this.partnerName,
    this.value,
    this.notes,
    this.articole,
    this.moduriDePlata
  }) ;

  factory PartenerElem.fromJson(Map<String, dynamic> parsedJson){
    var notes = parsedJson['Note']!=null?parsedJson['Note'] as List:null;
    var articoles = parsedJson['Articole']!=null?parsedJson['Articole'] as List:null;
    var moduriDePlata = parsedJson['Moduri_De_Plata']!=null?parsedJson['Moduri_De_Plata'] as List:null;
    List<NoteElem> notesList = notes!=null?notes.map((i) => NoteElem.fromJson(i)).toList():null;
    List<PartenerArticolElem> articoleList = articoles!=null? articoles.map((i) => PartenerArticolElem.fromJson(i)).toList():null;
    List<ModDePlataElem> moduriDePlataList = moduriDePlata!=null?moduriDePlata.map((i) => ModDePlataElem.fromJson(i)).toList():null;
    return new PartenerElem(
        partnerId: parsedJson['Partner_Id'],
        partnerName: parsedJson['Partner_Name'].toString(),
        value: parsedJson['Value'],
        notes: notesList,
        articole: articoleList,
        moduriDePlata: moduriDePlataList
    );
  }

  String getValueRoundedAsString(){
    if (value == null){
      return "";
    }
    return double.parse(value.toStringAsFixed(2)).toString();
  }
}
class NoteElem{
  final String timestamp;
  final double value;

  NoteElem({
    this.timestamp,
    this.value
  }) ;

  factory NoteElem.fromJson(Map<String, dynamic> parsedJson){
    return new NoteElem(
        timestamp: parsedJson['Timestamp'],
        value: parsedJson['Value']
    );
  }

}
class PartenerArticolElem{
  final String articolName;
  final double cantitate;
  final double vanzareBrut;
  final double castig;

  PartenerArticolElem({
    this.articolName,
    this.cantitate,
    this.vanzareBrut,
    this.castig
  }) ;

  factory PartenerArticolElem.fromJson(Map<String, dynamic> parsedJson){
    return new PartenerArticolElem(
        articolName: parsedJson['Articol_Name'],
        cantitate: parsedJson['Cantitate'],
        vanzareBrut: parsedJson['Vanzare_Brut'],
        castig: parsedJson['Castig']
    );
  }

}
class ModDePlataElem{
  final String title;
  final double value;

  ModDePlataElem({
    this.title,
    this.value
  }) ;

  factory ModDePlataElem.fromJson(Map<String, dynamic> parsedJson){
    return new ModDePlataElem(
        title: parsedJson['Title'],
        value: parsedJson['Value']
    );
  }

}
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/stornari_screen.dart';
import 'package:hostware_flutter/ui/incasari_screen.dart';

import 'package:hostware_flutter/ui/views/incasari_builder.dart';
import 'package:hostware_flutter/ui/views/incasari_diagram_builder.dart';

ScrollController scrollControllerIncasariDetail;

class IncasariDetailScreen extends StatefulWidget {
  List<GenericElem> data;

  IncasariDetailScreen(List<GenericElem> data_, {Key key}) : super(key: key) {
    data = data_;
  }

  @override
  IncasariDetailState createState() => IncasariDetailState(this.data);

}

class IncasariDetailState extends State<IncasariDetailScreen> {
//  List<MyData> myDatas;
  List<GenericElem> data;

  IncasariDetailState(this.data);
  @override
  void initState() {
//    initData();

    scrollControllerIncasariDetail = ScrollController(initialScrollOffset: 84);
    initStateCustome().then((_) {
      setState(() {});
      WidgetsBinding.instance.addPostFrameCallback((_) => executeAfterWholeBuildProcess(context));
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollControllerIncasariDetail.dispose();
    super.dispose();
  }

  // Init state logic
  Future initStateCustome() async {}

  _scrollToTop() {
    scrollControllerIncasariDetail.animateTo(scrollControllerIncasariDetail.position.minScrollExtent, duration: Duration(milliseconds: 100), curve: Curves.easeIn);
//    setState(() => _isOnTop = true);
  }

  void executeAfterWholeBuildProcess(BuildContext context) {
    // executes after build is done
    _scrollToTop();
  }

//  void initData() {
//    setState(() {
//      myDatas = new List();
//      myDatas.add(new MyData(
//        text: "Numerar",
//        value: "34993,98",
//      ));
//      myDatas.add(new MyData(
//        text: "Card",
//        value: "7261,13",
//      ));
//      myDatas.add(new MyData(
//        text: "Virament",
//        value: "151,30",
//      ));
//      myDatas.add(new MyData(
//        text: "Protocol",
//        value: "2859,71",
//      ));
//
//
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Incasari",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Expanded(
          flex: 1,
          child: Container(
              width: double.infinity,
              child: SingleChildScrollView(
                controller: scrollControllerIncasariDetail,
                child: Column(
                  children: <Widget>[
                    diagramBuilder(MediaQuery.of(context).size.width, data),
                    dashboardListBuilder(MediaQuery.of(context).size.width, context, data),
                  ],
                ),
              )))
    ])));
  }
}

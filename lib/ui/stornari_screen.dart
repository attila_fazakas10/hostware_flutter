import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/incasari_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

ScrollController scrollControllerStornari;

class StornariScreen extends StatefulWidget {
  List<GenericElem> data;

  StornariScreen(List<GenericElem> data_, {Key key}) : super(key: key) {
    data = data_;
  }

  @override
  StornariState createState() => StornariState(this.data);
}

class StornariState extends State<StornariScreen> {
//  List<MyData> myDatas;
  List<GenericElem> data;
  StornariState(this.data);
  @override
  void initState() {
//    initData();

    scrollControllerStornari = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerStornari.dispose();
    super.dispose();
  }
//
//  void initData() {
//    setState(() {
//      myDatas = new List();
//      for (int i = 0; i < 20; i++) {
//        myDatas.add(new MyData(
//          text: "Data text " + i.toString(),
//          value: "" + i.toString() + " lei",
//        ));
//      }
//    });
//  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Stornari",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerStornari,
                    itemExtent: kRowHeight40,
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Column(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                child: Container(
                                  height: kRowHeight40,
                                  width: MediaQuery.of(context).size.width,
                                  color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                      child: Text(data[index].title, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                      child: Text(NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data[index].value), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                    )
                                  ]),
                                )),
                          ],
                        ),
                      );
                    },
                  ),
                ),


                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
    ])));
  }
}

class MyData {
  String text;
  String value;

  MyData({this.text, this.value});
}

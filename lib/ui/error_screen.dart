import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:flutter/services.dart';
import 'package:connectivity/connectivity.dart';

import 'package:http/http.dart' as http;


class ErrorScreen extends StatefulWidget {
  String err;

  ErrorScreen(String err_, {Key key}) : super(key: key) {
    err = err_;
  }
  @override
  ErrorState createState() => ErrorState(this.err);
}

class ErrorState extends State<ErrorScreen> {
  String err;
  bool isInternetOn = true;

  ErrorState(this.err);

  @override
  void initState() {

    checkInternetConnectivity().then((isOnline) async {
      setState(() {
      if (isOnline) {
        isInternetOn = true;
      } else {
        isInternetOn = false;
      }});
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(

        child: Column (
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
              child: Container(
                  alignment: Alignment.centerRight,
                  width: MediaQuery.of(context).size.width,
                  child: new IconButton(
                    icon: new Icon(Icons.close, color: Theme.Colors.HWGreyTextColor),
                    onPressed: () => Navigator.pop(context, true),
                  )),
            ),
            Container(
                margin: EdgeInsets.only(top: kMargin30, bottom: kMargin30, left: kMargin30, right: kMargin30),
                child: isInternetOn?new Image( image: new AssetImage("assets/no_server.png")):new Image( image: new AssetImage("assets/no_connection.png"))),
            Text(isInternetOn?"Serverul nu raspunde":"Dispozitivul nu este\nconectat la retea", textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWrowGrey, fontSize: kFontSize24)),
            Container(
              height: kRowHeight68,
            ),
            new InkWell(
              onTap: () => _onTapRetry(context),
              child:new Container(
                  alignment: Alignment.center,
                  child: Container(
                      decoration: BoxDecoration(
                          color: Theme.Colors.HWrowGrey,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(
                            color: Theme.Colors.HWrowGrey,
                            width: 1,
                          )),
                      height: kRowHeight35,
                      width: (MediaQuery.of(context).size.width*0.4),
                      child: Align(
                        alignment: Alignment.center,child:Text("Incearca din nou", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),)
                  )),)
          ],
        )));
  }

  _onTapRetry(BuildContext context) {
    Navigator.pop(context, 'retry');
  }
  static Future<bool> checkInternetConnectivity() async {
    bool isConnected;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        isConnected = true;
      }
    } on SocketException catch (_) {
      isConnected = false;
    }
    return isConnected;
  }
}

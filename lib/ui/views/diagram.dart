import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:hostware_flutter/ui/incasari_screen.dart';
import 'package:hostware_flutter/ui/stornari_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutPieChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory DonutPieChart.withSampleData(List<GenericElem> data) {
    return new DonutPieChart(
      _createSampleData(data),
      // Disable animations for image tests.
      animate: true,
    );
  }


  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        animate: animate,
        // Configure the width of the pie slices to 60px. The remaining space in
        // the chart will be left as a hole in the center.
        defaultRenderer: new charts.ArcRendererConfig( arcWidth: 40, strokeWidthPx: 0.0,
                                                        arcRendererDecorators: [
                                                          new charts.ArcLabelDecorator(
                                                            labelPosition: charts.ArcLabelPosition.auto, labelPadding: 3, showLeaderLines: false, outsideLabelStyleSpec: charts.TextStyleSpec( fontFamily: "QuicksandBold", color: charts.Color.fromHex(code:"#38ba37"), ), ),
                                                        ], ));
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createSampleData(List<GenericElem> data) {
    int atWorkNr=85;
    int missingNr=6;
    int lateNr=2;
    int breakNr=3;

    double ossz = 0;
    for ( GenericElem genericElem in data){
      ossz+=genericElem.value.abs();
    }

//
//    final List<LinearSales> itemList = new List();
//    if (data.length > 0 && data[0]!=null && data[0].value > 0) {
//      itemList.add(new LinearSales(0, data[0].value.round() , data[0].getValueAsString()+((data[0].value <10)?"":"%"), charts.Color.fromHex(code: "#219653"), charts.TextStyleSpec(fontFamily: "QuicksandBold", color: charts.Color.fromHex(code:"#000000"))));
//    }
//    if (data.length > 1 && data[1]!=null && data[1].value > 0) {
//      itemList.add(new LinearSales(1, data[1].value.round(), data[1].getValueAsString()+((data[1].value <10)?"":"%"), charts.Color.fromHex(code: "#40D1DB"), charts.TextStyleSpec( fontFamily: "QuicksandBold",color: charts.Color.fromHex(code:"#000000"))));
//    }
//    if (data.length > 2 && data[2]!=null && data[2].value > 0) {
//        itemList.add(new LinearSales(2, data[2].value.round(), data[2].getValueAsString()+((data[2].value<10)?"":"%"), charts.Color.fromHex(code: "#00499E"), charts.TextStyleSpec( fontFamily: "QuicksandBold",color: charts.Color.fromHex(code:"#000000"))));
//    }
//    if (data.length > 3 && data[3]!=null && data[3].value > 0) {
//          itemList.add(new LinearSales(3, data[3].value.round(), data[3].getValueAsString()+((data[3].value<10)?"":"%"), charts.Color.fromHex(code: "#DB8585"), charts.TextStyleSpec( fontFamily: "QuicksandBold",color: charts.Color.fromHex(code:"#000000"))));
//    }
print('ABSOLUTE OSSZ:'+ossz.toString());
    final List<LinearSales> itemList = new List();
    if (data.length > 0 && data[0]!=null && data[0].value.abs() > 0) {
      int percentage = ((data[0].value.abs() * 100)/ossz).toInt();
      itemList.add(new LinearSales(0, percentage>0?percentage:1 ,percentage>3? percentage.toString()+((percentage <10)?"":"%"):"", charts.Color.fromHex(code: "#219653"), charts.TextStyleSpec(fontFamily: "QuicksandBold", color: charts.Color.fromHex(code:"#000000"))));
    }
    if (data.length > 1 && data[1]!=null && data[1].value.abs() > 0) {
      int percentage = ((data[1].value.abs() * 100)/ossz).toInt();
      itemList.add(new LinearSales(1,  percentage>0?percentage:1 ,percentage>3? percentage.toString()+((percentage <10)?"":"%"):"", charts.Color.fromHex(code: "#40D1DB"), charts.TextStyleSpec( fontFamily: "QuicksandBold",color: charts.Color.fromHex(code:"#000000"))));
    }
    if (data.length > 2 && data[2]!=null && data[2].value.abs() > 0) {
      int percentage = ((data[2].value.abs() * 100)/ossz).toInt();
        itemList.add(new LinearSales(2,  percentage>0?percentage:1 , percentage>3? percentage.toString()+((percentage <10)?"":"%"):"", charts.Color.fromHex(code: "#00499E"), charts.TextStyleSpec( fontFamily: "QuicksandBold",color: charts.Color.fromHex(code:"#000000"))));
    }
    if (data.length > 3 && data[3]!=null && data[3].value.abs() > 0)  {
      int percentage = ((data[3].value.abs() * 100)/ossz).toInt();
          itemList.add(new LinearSales(3,  percentage>0?percentage:1 ,percentage>3?  percentage.toString()+((percentage <10)?"":"%"):"", charts.Color.fromHex(code: "#DB8585"), charts.TextStyleSpec( fontFamily: "QuicksandBold",color: charts.Color.fromHex(code:"#000000"))));
    }


    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
//
        domainFn: (LinearSales sales, _) => sales.year,
        domainLowerBoundFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        labelAccessorFn: ( LinearSales sales, _) => sales.name,
        insideLabelStyleAccessorFn: ( LinearSales sales, _) => sales.textStyleSpec,
//        fillColorFn: (LinearSales sales, _) => sales.color,
        colorFn: (LinearSales sales, _) =>  sales.color,
//        areaColorFn:(LinearSales sales, _) =>  sales.color,
        data: itemList,
      )
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;
  final String name;
  final charts.Color color;
  final charts.TextStyleSpec textStyleSpec;

  LinearSales(this.year, this.sales, this.name, this.color, this.textStyleSpec);
}
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/incasari_screen.dart';
import 'package:hostware_flutter/ui/stornari_screen.dart';
import 'dart:async';
import 'package:hostware_flutter/utils/constants.dart';
import 'package:intl/intl.dart';

dashboardListBuilder(val, context, List<GenericElem> data) {
  return Container(
    margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
    child: Column(
      children: [
        Padding(
            padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
            child: Row(children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                    height: kRowHeight68,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.Colors.HWrowGrey,
                    margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                    child: Stack(
                      children: <Widget>[
                        new Align(
                            alignment: Alignment.topLeft,
                            child: Container (
                              width: 6.0,
                              height: kRowHeight68/2,
                              color: Theme.Colors.HWPaletteGreen1,
                            )),
                        Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Center(
                            child: Text(data.length > 0 ?data[0].title:"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          ),
                          Center(child: Text("" + (data.length > 0 ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data[0].value):"" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14))),
                        ]),
                      ],
                    )),
              ),
              Expanded(
                flex: 1,
                child: Container(
                    height: kRowHeight68,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.Colors.HWrowGrey,
                    margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 0.0),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topLeft,
                            child: Container (
                              width: 6.0,
                              height: kRowHeight68/2,
                              color: Theme.Colors.HWPaletteBlue1,
                            )),
                        Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Center(
                            child: Text(data.length > 1 ?data[1].title:"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          ),
                          Center(child: Text("" + (data.length > 1 ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data[1].value):"" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14))),
                        ]),
                      ],
                    )),
              ),
            ])),
        Padding(
            padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
            child: Row(children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                    height: kRowHeight68,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.Colors.HWrowGrey,
                    margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topLeft,
                            child: Container (
                              width: 6.0,
                              height: kRowHeight68/2,
                              color: Theme.Colors.HWPaletteBlue2,
                            )),
                        Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Center(
                            child: Text(data.length > 2 ?data[2].title:"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          ),
                          Center(child: Text("" + (data.length >2 ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data[2].value):"" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14))),
                        ]),
                      ],
                    )),
              ),
              Expanded(
                flex: 1,
                child: Container(
                    height: kRowHeight68,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.Colors.HWrowGrey,
                    margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 0.0),
                    child: Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topLeft,
                            child: Container (
                              width: 6.0,
                              height: kRowHeight68/2,
                              color: Theme.Colors.HWPaletteOrange1,
                            )),
                        Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Center(
                            child: Text(data.length > 3 ?data[3].title:"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          ),
                          Center(child: Text("" +(data.length > 3 ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data[3].value):"" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize14))),
                        ]),
                      ],
                    )),
              ),
            ])),
      ],
    ),
  );
}

_lineItemBuild(val) {
  return Container(
    margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 35.0, right: 0.0),
    color: Theme.Colors.loginDarkGrey,
    height: 1,
    width: val,
  );
}

_personsItemBuild(val) {
  return Container(
    child: Text(
      (val < 2) ? val.toString() + " person" : val.toString() + " persons",
      style: TextStyle(color: Theme.Colors.loginDarkGrey, fontWeight: FontWeight.normal, fontSize: 12),
    ),
    alignment: Alignment.centerRight,
  );
}

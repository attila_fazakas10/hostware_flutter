import 'package:flutter/material.dart';
import 'package:hostware_flutter/ui/incasari_screen.dart';
import 'package:hostware_flutter/ui/views/diagram.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/stornari_screen.dart';
import 'package:flutter/services.dart';


diagramBuilder(val,List<GenericElem> data) {
  return Container(
    margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 10.0, right: 10.0),
    height: (val/3)*2,
    width: (val/3)*2,
    child: DonutPieChart.withSampleData(data),
  );
}

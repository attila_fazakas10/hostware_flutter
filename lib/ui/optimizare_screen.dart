import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/optimizare_settings.dart';
import 'package:hostware_flutter/ui/optimizare_detail_screen.dart';
import 'package:flutter/services.dart';

ScrollController scrollControllerOptimizare;

class OptimizareScreen extends StatefulWidget {

  OptimizareScreen( {Key key}) : super(key: key) {
  }

  @override
  OptimizareState createState() => OptimizareState();
}

class OptimizareState extends State<OptimizareScreen> {
  List<MyDataOptimizare> myDatas;

  bool _puzzleSwitched = true;
  bool _dogSwitched = true;
  bool _plowhorseSwitched = true;
  bool _starSwitched = true;

  OptimizareState();

  @override
  void initState() {
    initData();

    scrollControllerOptimizare = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerOptimizare.dispose();
    super.dispose();
  }

  void initData() {
    setState(() {
      myDatas = new List();
      myDatas.add(new MyDataOptimizare(text: "Napimenu" , value:34, value2: 50, category: "Star"));
      myDatas.add(new MyDataOptimizare(text: "Babfozelek" , value:62, value2: 77, category: "Puzzle"));
      myDatas.add(new MyDataOptimizare(text: "Rostelyos" , value:80, value2: 95, category: "Dog"));
      myDatas.add(new MyDataOptimizare(text: "Galambsziv martas" , value:70, value2: 44, category: "Plowhorse"));

      myDatas.add(new MyDataOptimizare(text: "Paprikas" , value:62, value2: 77, category: "Dog"));
      myDatas.add(new MyDataOptimizare(text: "Gomboccsorba" , value:80, value2: 95, category: "Star"));
      myDatas.add(new MyDataOptimizare(text: "Valamilyen martas izével" , value:70, value2: 44, category: "Puzzle"));
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
              Container(
                height: kBarHeight,
                color: Theme.Colors.HWwhite,
                child: new Container(
                  color: Theme.Colors.HWbarGrey,
                  height: kBarHeight,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: kMargin20),
                        child: Container(
                          child: Text(
                            "Optimizare profit",
                            style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                        child: Container(
                            alignment: Alignment.centerLeft,
                            height: kBarHeight,
                            width: kBarHeight,
                            child: new IconButton(
                                icon: new Icon(Icons.tune, color: Theme.Colors.HWwhite),
                                onPressed: () {
                                  _onTapSettings(context);
                                }
                            )),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: kMargin10,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                color: Theme.Colors.HWwhite,
              ),
              Column(
                children: <Widget>[
                  new Container(
                    color: Theme.Colors.HWwhite,
                    height: kRowHeight35,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    child: new Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 22,
                                width: 15,
                                margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin4),
                                color: Theme.Colors.HWPaletteBlue1,
                              ),
                              Text(
                                "Plowhorse",
                                style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Switch(
                              activeColor: Theme.Colors.HWPaletteBlue1,
                              inactiveThumbColor: Theme.Colors.HWgreyLightest,
                              inactiveTrackColor: Theme.Colors.HWSwitchColor,
                              value: _plowhorseSwitched,
                              onChanged: (bool newValue) {
                                setState(() {
                                  _plowhorseSwitched = newValue;
                                  switchStateChanged();
                                });
                              },
                            )
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 22,
                                width: 15,
                                margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin4),
                                color: Theme.Colors.HWPaletteGreen1,
                              ),
                              Text(
                                "Star",
                                style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Switch(
                              activeColor: Theme.Colors.HWPaletteGreen1,
                              inactiveThumbColor: Theme.Colors.HWgreyLightest,
                              inactiveTrackColor: Theme.Colors.HWSwitchColor,
                              value: _starSwitched,
                              onChanged: (bool newValue) {
                                setState(() {
                                  _starSwitched = newValue;
                                  switchStateChanged();
                                });
                              },
                            )
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    color: Theme.Colors.HWwhite,
                    height: kRowHeight35,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    child: new Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 22,
                                width: 15,
                                margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin4),
                                color: Theme.Colors.HWPaletteBlue2,
                              ),
                              Text(
                                "Puzzle",
                                style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Switch(
                              activeColor: Theme.Colors.HWPaletteBlue2,
                              inactiveThumbColor: Theme.Colors.HWgreyLightest,
                              inactiveTrackColor: Theme.Colors.HWSwitchColor,
                              value: _puzzleSwitched,
                              onChanged: (bool newValue) {
                                setState(() {
                                  _puzzleSwitched = newValue;
                                  switchStateChanged();
                                });
                              },
                            )
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 22,
                                width: 15,
                                margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin20, right: kMargin4),
                                color: Theme.Colors.HWPaletteOrange1,
                              ),
                              Text(
                                "Dog",
                                style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize12),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Switch(
                              activeColor: Theme.Colors.HWPaletteOrange1,
                              inactiveThumbColor: Theme.Colors.HWgreyLightest,
                              inactiveTrackColor: Theme.Colors.HWSwitchColor,
                              value: _dogSwitched,
                              onChanged: (bool newValue) {
                                setState(() {
                                  _dogSwitched = newValue;
                                  switchStateChanged();
                                });
                              },
                            )
                        ),
                      ],
                    ),
                  )
                ],
              ),

              Container(
                height: kMargin10,
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                color: Theme.Colors.HWwhite,
              ),
              Container(
                height: kRowHeight23,
                color: Theme.Colors.HWrowGrey,
                child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: Container(),
                  ),

                  Expanded(
                      flex: 4,
                      child: Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                            child: Text("Vanzare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          ))),

                  Expanded(
                      flex: 4,
                      child: Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                            child: Text("Castig", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          )))
                ]),
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      children: <Widget>[
                        new Expanded(
                          child: new ListView.builder(
                            padding: EdgeInsets.all(0.0),
                            controller: scrollControllerOptimizare,
                            itemExtent: kRowHeight40,
                            itemCount: myDatas.length,
                            itemBuilder: (context, index) {
                              return Container(
                                padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                                margin: EdgeInsets.symmetric(vertical: 0.0),
                                /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                                child: Padding(
                                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                    child: InkWell(
                                        onTap: () => _onTapItem(context, myDatas[index]),
                                        child: Container(
                                          height: kRowHeight40,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                                          child: Stack(children: <Widget>[

                                            new Align(
                                                alignment: Alignment.centerLeft,
                                                child: Container(
                                                  height: kRowHeight40,
                                                  color: (myDatas[index].category == "Star") ? Theme.Colors.HWPaletteGreen1 : (myDatas[index].category == "Puzzle")
                                                      ? Theme.Colors.HWPaletteBlue2
                                                      : (myDatas[index].category == "Dog") ? Theme.Colors.HWPaletteOrange1 : Theme.Colors.HWPaletteBlue1,
                                                  width: 5,
                                                )),
                                            new Align(
                                                alignment: Alignment.bottomRight,
                                                child: Image.asset(
                                                  "assets/img/polygon_small.png",
                                                  width: 14.0,
                                                  height: 14.0,
//                                      fit: BoxFit.cover,
                                                )),
                                            Center(
                                              child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                Expanded(
                                                  flex: 6,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                                    child: Text(myDatas[index].text, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 4,
                                                  child: Align(
                                                      alignment: Alignment.centerRight,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                                        child: Text(myDatas[index].value.toString() + "%", style: TextStyle(fontWeight: FontWeight.normal,
                                                            color: Theme.Colors.HWBlackTextColor,
                                                            fontSize: kFontSize14)),
                                                      )),
                                                ),
                                                Expanded(
                                                  flex: 4,
                                                  child: Align(
                                                      alignment: Alignment.centerRight,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                                        child: Text(myDatas[index].value2.toString() + "%", style: TextStyle(fontWeight: FontWeight.normal,
                                                            color: Theme.Colors.HWred,
                                                            fontSize: kFontSize14)),
                                                      )),
                                                )
                                              ]),
                                            )
                                          ]),
                                        ))),
                              );
                            },
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                            child: Container(
                              height: 1.0,
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,
                              color: Theme.Colors.HWwhite,
                            )),
                      ],
                    ),
                  ))
            ])));
  }

  void _onTapItem(BuildContext context, MyDataOptimizare data) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              OptimizareDetailScreen(data)),
    );
  }
  void _onTapSettings(BuildContext context) {
    Navigator.push(context,MaterialPageRoute(builder: (context) =>OptimizareSettingsScreen()),
    );
  }

  void switchStateChanged() {
    myDatas = new List();
    if (_starSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Napimenu", value: 34, value2: 50, category: "Star"));
    }
    if (_puzzleSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Babfozelek" , value:62, value2: 77, category: "Puzzle"));
    }
    if (_dogSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Rostelyos" , value:80, value2: 95, category: "Dog"));
    }
    if (_plowhorseSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Galambsziv martas" , value:70, value2: 44, category: "Plowhorse"));
    }
    if (_dogSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Paprikas", value: 62, value2: 77, category: "Dog"));
    }
    if (_starSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Gomboccsorba", value: 80, value2: 95, category: "Star"));
    }
    if (_puzzleSwitched) {
      myDatas.add(new MyDataOptimizare(text: "Valamilyen martas izével", value: 70, value2: 44, category: "Puzzle"));
    }
  }

}

class MyDataOptimizare {
  String text;
  String category;
  int value;
  int value2;

  MyDataOptimizare({this.text,this.category, this.value, this.value2});
}

import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:hostware_flutter/ui/centralizator_screen.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:date_format/date_format.dart';

ScrollController scrollControllerCentralizatorOspatar;

class CentralizatorOspatarScreen extends StatefulWidget {
  CentralizatorIncasariElem centralizatorIncasariElem;


  CentralizatorOspatarScreen(CentralizatorIncasariElem centralizatorIncasariElem_, {Key key}) : super(key: key) {
    centralizatorIncasariElem = centralizatorIncasariElem_;
  }

  @override
  CentralizatorOspatarState createState() => CentralizatorOspatarState(this.centralizatorIncasariElem);
}

class CentralizatorOspatarState extends State<CentralizatorOspatarScreen> {
  CentralizatorIncasariElem centralizatorIncasariElem;
  List<bool> openedStatusList;
  bool allOpened;
  CentralizatorOspatarState(this.centralizatorIncasariElem );

  @override
  void initState() {


    scrollControllerCentralizatorOspatar = ScrollController();
    setState(() {
      allOpened = false;
      if (centralizatorIncasariElem!=null && centralizatorIncasariElem.incasariDetails!=null ) {
        openedStatusList = new List();
        for (IncasariDetailElem incasariDetailElem in centralizatorIncasariElem.incasariDetails) {
          openedStatusList.add(false);
        }
        if (openedStatusList.length>0){
          openedStatusList[0] = true;
        }

      }

    });
    super.initState();
  }

  @override
  void dispose() {
    scrollControllerCentralizatorOspatar.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    centralizatorIncasariElem!=null?centralizatorIncasariElem.ospatarName:"",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
              dashboardListBuilder(MediaQuery.of(context).size.width, context, centralizatorIncasariElem),
      Container(
        height: kMargin10,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),

              Expanded(
                  flex: 1,
                  child: Container(
                    color: Theme.Colors.HWwhite,
                    width: double.infinity,
                    child: Column(
                      children: <Widget>[
                        new Expanded(
                          child: new ListView.builder(
                            padding: EdgeInsets.all(0.0),
                            controller: scrollControllerCentralizator,
//                            itemExtent: 6*(kRowHeight68/2)+4,
                            itemCount: (centralizatorIncasariElem!=null && centralizatorIncasariElem.incasariDetails!=null)?centralizatorIncasariElem.incasariDetails.length:0,
                            itemBuilder: (context, index) {
                              return Container(
                                padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                                margin: EdgeInsets.symmetric(vertical: 0.0),
                                /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                                child: Padding(
                                    padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: 0.0, right: 0.0),
                                    child: InkWell(
                                        onTap: () => _onTapItem(context,index),
                                        child: Column(
                                            children: <Widget>[

                                              Container(
                                                height: kRowHeight68/2,
                                                width: MediaQuery.of(context).size.width,
                                                color: Theme.Colors.HWlightestGrey,
                                                child: Stack(children: <Widget>[

                                                  Center(
                                                    child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                      Expanded(
                                                        flex: 6,
                                                        child: Padding(
                                                          padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                                          child: Text(convertDateFromString(centralizatorIncasariElem.incasariDetails[index].date.split(" ")[0]), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                                        ),
                                                      ),

                                                      new Align(
                                                          alignment: Alignment.centerRight,
                                                          child: openedStatusList[index]?Image.asset(
                                                            "assets/img/down_arrow.png",
                                                            width: 20.0,
                                                            height: 20.0,
                                                          ):Image.asset(
                                                            "assets/img/left_arrow.png",
                                                            width: 20.0,
                                                            height: 20.0,
                                                          )),
                                                    ]),
                                                  )
                                                ]),
                                              ),
                                              Container(
                                                  height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?5*(kRowHeight68/2)+4:0,
                                                  padding: EdgeInsets.only(top: 0, bottom: kMargin4, left: 0, right: 0),
                                                  width: MediaQuery.of(context).size.width,
                                                  color: Theme.Colors.HWwhite,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Container(
                                                          child: Row(
                                                              children: <Widget>[

                                                                Container(
                                                                  height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                  width: MediaQuery.of(context).size.width/6,
                                                                  child: new Align(
                                                                      alignment: Alignment.topLeft,
                                                                      child: Container (
                                                                        width: 6.0,
                                                                        height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                        color: Theme.Colors.HWPaletteBlue2,
                                                                      )),

                                                                ),
                                                                Container(
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    width: 2*(MediaQuery.of(context).size.width/6),
                                                                    child: Align(
                                                                      alignment: Alignment.centerLeft,
                                                                      child:Text("Numerar", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                ),
                                                                Container(
                                                                    padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0, right: kMargin10),
                                                                    width: 3*(MediaQuery.of(context).size.width/6),
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    child: Align(
                                                                      alignment: Alignment.centerRight,
                                                                      child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariElem.incasariDetails[index].numerar), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                )
                                                              ])
                                                      ),
                                                      Container(
                                                          child: Row(
                                                              children: <Widget>[

                                                                Container(
                                                                  height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                  width: MediaQuery.of(context).size.width/6,
                                                                  child: new Align(
                                                                      alignment: Alignment.topLeft,
                                                                      child: Container (
                                                                        width: 6.0,
                                                                        height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                        color: Theme.Colors.HWPaletteOrange1,
                                                                      )),

                                                                ),
                                                                Container(
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    width: 2*(MediaQuery.of(context).size.width/6),
                                                                    child: Align(
                                                                      alignment: Alignment.centerLeft,
                                                                      child:Text("Card", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                ),
                                                                Container(
                                                                    padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0, right: kMargin10),
                                                                    width: 3*(MediaQuery.of(context).size.width/6),
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    child: Align(
                                                                      alignment: Alignment.centerRight,
                                                                      child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariElem.incasariDetails[index].card), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                )
                                                              ])
                                                      ),
                                                      Container(
                                                          child: Row(
                                                              children: <Widget>[

                                                                Container(
                                                                  height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    width: MediaQuery.of(context).size.width/6,
                                                                  child: new Align(
                                                                      alignment: Alignment.topLeft,
                                                                      child: Container (
                                                                        width: 6.0,
                                                                        height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                        color: Theme.Colors.HWPaletteGreen1,
                                                                      )),

                                                                ),
                                                                Container(
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    width: 2*(MediaQuery.of(context).size.width/6),
                                                                    child: Align(
                                                                      alignment: Alignment.centerLeft,
                                                                      child:Text("Virament", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                ),
                                                                Container(
                                                                    padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0, right: kMargin10),
                                                                    width: 3*(MediaQuery.of(context).size.width/6),
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    child: Align(
                                                                      alignment: Alignment.centerRight,
                                                                      child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariElem.incasariDetails[index].virament), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                )
                                                              ])
                                                      ),
                                                      Container(
                                                          child: Row(
                                                              children: <Widget>[

                                                                Container(
                                                                  height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                  width: MediaQuery.of(context).size.width/6,
                                                                  child: new Align(
                                                                      alignment: Alignment.topLeft,
                                                                      child: Container (
                                                                        width: 6.0,
                                                                        height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                        color: Theme.Colors.HWPaletteBlue1,
                                                                      )),

                                                                ),
                                                                Container(
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    width: 2*(MediaQuery.of(context).size.width/6),
                                                                    child: Align(
                                                                      alignment: Alignment.centerLeft,
                                                                      child:Text("Tichete", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                ),
                                                                Container(
                                                                    padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0, right: kMargin10),
                                                                    width: 3*(MediaQuery.of(context).size.width/6),
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    child: Align(
                                                                      alignment: Alignment.centerRight,
                                                                      child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariElem.incasariDetails[index].tichete), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                )
                                                              ])
                                                      ),
                                                      Container(
                                                          child: Row(
                                                              children: <Widget>[

                                                                Container(
                                                                  height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                  width: MediaQuery.of(context).size.width/6,
                                                                  child: new Align(
                                                                      alignment: Alignment.topLeft,
                                                                      child: Container (
                                                                        width: 6.0,
                                                                        height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                        color: Theme.Colors.HWPaletteOrange2,
                                                                      )),

                                                                ),
                                                                Container(
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    width: 2*(MediaQuery.of(context).size.width/6),
                                                                    child: Align(
                                                                      alignment: Alignment.centerLeft,
                                                                      child:Text("Altele", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                ),
                                                                Container(
                                                                    padding: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0, right: kMargin10),
                                                                    width: 3*(MediaQuery.of(context).size.width/6),
                                                                    height: (openedStatusList!=null && openedStatusList.length>index && openedStatusList[index])?kRowHeight68/2:0,
                                                                    child: Align(
                                                                      alignment: Alignment.centerRight,
                                                                      child:Text(NumberFormat.currency(locale: 'eu', symbol: '').format(centralizatorIncasariElem.incasariDetails[index].altele), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),)
                                                                )
                                                              ])
                                                      ),
                                                    ],
                                                  )
                                              )
                                            ]))),
                              );
                            },
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                            child: Container(
                              height: 1.0,
                              width: MediaQuery.of(context).size.width,
                              color: Theme.Colors.HWwhite,
                            )),
                      ],
                    ),
                  ))
    ])));
  }
  String convertDateFromString(String strDate){
    DateFormat inputFormat = DateFormat("dd/MM/yyyy");
    DateTime dateTime = inputFormat.parse(strDate);
    print('DATE:::::'+formatDate(dateTime, [yyyy, '/', mm, '/', dd, ' ', hh, ':', nn, ':', ss]));
    return formatDate(dateTime, [dd, '.', mm, '.', yyyy]);
  }
  void _onTapItem(BuildContext context, int idx) {

    setState(() {


//      if (openedStatusList[idx]) {
        openedStatusList[idx] = !openedStatusList[idx];
        /* } else {
          for (int i=0;i<openedStatusList.length;i++) {
            openedStatusList[i] = false;
          }
          openedStatusList[idx] = !openedStatusList[idx];

      }*/

    });
  }
  void _onTapAll(BuildContext context) {

    setState(() {


      if (openedStatusList.contains(true)) {
        for (int i=0;i<openedStatusList.length;i++) {
          openedStatusList[i] = false;
        }
      } else {
          for (int i=0;i<openedStatusList.length;i++) {
            openedStatusList[i] = true;
          }

      }

    });
  }
  double _calculateWidthPercentage(double value, double total) {

    return ((MediaQuery.of(context).size.width-(kMargin4*2))*value)/(total);

  }
  dashboardListBuilder(val, context, CentralizatorIncasariElem data) {
    return Container(
      margin: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
      child: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
              child: Row(children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                      height: kRowHeight68,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWrowGrey,
                      margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                      child: Stack(
                        children: <Widget>[
                          new Align(
                              alignment: Alignment.topLeft,
                              child: Container (
                                width: 6.0,
                                height: kRowHeight68/2,
                                color: Theme.Colors.HWPaletteBlue2,
                              )),
                          Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Center(
                              child: Text("Numerar", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                            ),
                            Center(child: Text("" + (data.totalNumerar !=null ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data.totalNumerar):"-" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                          ]),
                        ],
                      )),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                      height: kRowHeight68,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWrowGrey,
                      margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 0.0),
                      child: Stack(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topLeft,
                              child: Container (
                                width: 6.0,
                                height: kRowHeight68/2,
                                color: Theme.Colors.HWPaletteOrange1,
                              )),
                          Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Center(
                              child: Text("Card", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                            ),
                            Center(child: Text("" + (data.totalCard !=null ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data.totalCard):"-" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                          ]),
                        ],
                      )),
                ),
              ])),
          Padding(
              padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
              child: Row(children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                      height: kRowHeight68,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWrowGrey,
                      margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                      child: Stack(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topLeft,
                              child: Container (
                                width: 6.0,
                                height: kRowHeight68/2,
                                color: Theme.Colors.HWPaletteGreen1,
                              )),
                          Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Center(
                              child: Text("Virament", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                            ),
                            Center(child: Text("" + (data.totalVirament !=null ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data.totalVirament):"-" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                          ]),
                        ],
                      )),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                      height: kRowHeight68,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWrowGrey,
                      margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 0.0),
                      child: Stack(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topLeft,
                              child: Container (
                                width: 6.0,
                                height: kRowHeight68/2,
                                color: Theme.Colors.HWPaletteBlue1,
                              )),
                          Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Center(
                              child: Text("Tichete", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                            ),
                            Center(child: Text("" +(data.totalTichete !=null ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data.totalTichete):"-" ), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                          ]),
                        ],
                      )),
                ),
              ])),
          Padding(
              padding: EdgeInsets.only(top: 1.0, bottom: 0.0, left: kMargin4, right: kMargin4),
              child: Row(children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                      height: kRowHeight68,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWrowGrey,
                      margin: EdgeInsets.only(top: 0.0, bottom: 1.0, left: 0.0, right: 1.0),
                      child: Stack(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.topLeft,
                              child: Container (
                                width: 6.0,
                                height: kRowHeight68/2,
                                color: Theme.Colors.HWPaletteOrange2,
                              )),
                          Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Center(
                              child: Text("Altele", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                            ),
                            Center(child: Text("" + (data.totalAltele !=null ?NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data.totalAltele):"-"), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize24))),
                          ]),
                        ],
                      )),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    height: kRowHeight68,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.Colors.HWwhite,
                    child: InkWell(
                      onTap: () => _onTapAll(context),
                      child: new Align(
                        alignment: Alignment.bottomRight,
                        child: openedStatusList.contains(true)? Image.asset(
                          "assets/img/down_arrow.png",
                          width: 20.0,
                          height: 20.0,
                        ):Image.asset(
                          "assets/img/left_arrow.png",
                          width: 20.0,
                          height: 20.0,
                        )),)
                  ),
                ),
              ])),
        ],
      ),
    );
  }

}


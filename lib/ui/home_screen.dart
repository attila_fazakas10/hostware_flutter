import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/incasari_screen.dart';
import 'package:hostware_flutter/ui/parteneri_screen.dart';
import 'package:hostware_flutter/ui/monitoring_screen.dart';
import 'package:hostware_flutter/ui/optimizare_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';


ScrollController scrollControllerHome;

class HomeScreen extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<HomeScreen> {

  @override
  void initState() {
    scrollControllerHome = ScrollController(initialScrollOffset: 84);
    initStateCustome().then((_) {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollControllerHome.dispose();
    super.dispose();
  }

  // Init state logic
  Future initStateCustome() async {

  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        bottomNavigationBar: menu(),
        body: SafeArea(
            child: TabBarView(
          children: [IncasariScreen(), ParteneriScreen(), OptimizareScreen(), MonitoringScreen(), ],
        )),
      ),
    );
  }

  Widget menu() {
    return Container(
      color: Color(0xFF3A444F),
      child: TabBar(
        labelColor:  Color(0xFFB31A41) ,
        unselectedLabelColor: Colors.white,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorPadding: EdgeInsets.all(7.0),
        indicatorColor: Colors.white ,
        tabs: [
          Tab(
            text: "Incasari",
              icon: Image.asset( "assets/img/home.png",
                width: kBottomBarIconSize,
                height: kBottomBarIconSize,
              )
          ),
          Tab(
            text: "Parteneri",
              icon: Image.asset( "assets/img/users.png",
                width: kBottomBarIconSize,
                height: kBottomBarIconSize,
              )
          ),
          Tab(
            text: "Optimizare",
            icon: Image.asset( "assets/img/crosshair.png",
              width: kBottomBarIconSize,
              height: kBottomBarIconSize,
            ),
          ),
          Tab(
            text: "Monitoring",
            icon: Image.asset( "assets/img/activity.png",
              width: kBottomBarIconSize,
              height: kBottomBarIconSize,
            ),
          ),
        ],
      ),
    );
  }

}
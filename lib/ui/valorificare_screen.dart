import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/valorificare_detail_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

ScrollController scrollControllerValorificare;

class ValorificareScreen extends StatefulWidget {
  String period;

  ValorificareScreen(String period_, {Key key}) : super(key: key) {
    period = period_;
  }
  @override
  ValorificareState createState() => ValorificareState(this.period);
}

class ValorificareState extends State<ValorificareScreen> {
  String period;

  String totalVanzare;
  String totalCastig;
  List<ArticolCategoryElem> articolCategoriesList;

  ValorificareState(this.period);

  @override
  void initState() {
    initData();

    scrollControllerValorificare = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerValorificare.dispose();
    super.dispose();
  }

  void initData() {
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetVanzareArticolCategories(context, url, period != null ? period : "").then((response) {
        setState(() {
          if (response != null) {
            print('makeGetVanzareArticolCategories hivas nem null ');
            totalVanzare = response.totalVanzare != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.totalVanzare) : "-";
            totalCastig = response.totalCastig != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.totalCastig) : "-";

            articolCategoriesList = response.articolCategories;
            if (articolCategoriesList==null){
              articolCategoriesList = new List<ArticolCategoryElem>();
            }
          } else {
            print('initstate hivas  null ');
            totalVanzare = "";
            totalCastig = "";
            articolCategoriesList = new List<ArticolCategoryElem>();
            this.callbackErr("server");
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {
          articolCategoriesList = new List<ArticolCategoryElem>();
          totalVanzare = "";
          totalCastig= "";
        });

      }
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Valorificare articole",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
              /*Container(
                height: kMargin20,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWwhite,
              ),*/
              /*Container (
                color: Theme.Colors.HWwhite,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                height:  kRowHeight40 ,
                child: Text( "Minden unitate-hoz tartozo elem szerepel a listaban." , style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWGreyTextColor, fontSize: kFontSize14)),
              ),*/
              Container(
                height: kMargin20,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWwhite,
              ),
              Container(
                height: kRowHeight23,
                child: new Container(
                  color: Theme.Colors.HWwhite,
                  height: kRowHeight23,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: kMargin20),
                        child: Container(
                          child: Text(
                            "Vanzare pentru toate unitati",
                            style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.bold, fontSize: kFontSize14),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin20),
                        child: Container(
                          child: totalVanzare!=null?Text(
                            totalVanzare,
                            style: TextStyle(color: Theme.Colors.HWTextColor, fontWeight: FontWeight.bold, fontSize: kFontSize14),
                          ): Container(
                              margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                              width: kProgressSize,
                              height: kProgressSize,
                              child: CircularProgressIndicator(
                                backgroundColor: Theme.Colors.HWwhite,
                                strokeWidth: 1,
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: kRowHeight23,
                child: new Container(
                  color: Theme.Colors.HWwhite,
                  height: kRowHeight23,
                  width: MediaQuery.of(context).size.width,
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: kMargin20),
                        child: Container(
                          child: Text(
                            "Castig pentru toate unitati",
                            style: TextStyle(color: Theme.Colors.HWred, fontWeight: FontWeight.bold, fontSize: kFontSize14),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin20),
                        child: Container(
                          child: totalCastig!=null?Text(
                            totalCastig,
                            style: TextStyle(color: Theme.Colors.HWred, fontWeight: FontWeight.bold, fontSize: kFontSize14),
                          ):Container(
                              margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                              width: kProgressSize,
                              height: kProgressSize,
                              child: CircularProgressIndicator(
                                backgroundColor: Theme.Colors.HWwhite,
                                strokeWidth: 1,
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: kMargin20,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWwhite,
              ),
              Container(
                height: kRowHeight23,
                color: Theme.Colors.HWrowGrey,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 6,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 4,
                      child: Align(
                          alignment: Alignment.centerRight,
                          child:Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                            child: Text("Vanzare brut", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                          )),
                    ),
                    Expanded(
                        flex: 4,
                        child: Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                              child: Text("Castig", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                            ))
                    )

                  ]),
              ),
      articolCategoriesList!=null&&articolCategoriesList.length>0?
      Expanded(
          flex: 1,
          child:  Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerValorificare,
                    itemExtent: kRowHeight40,
                    itemCount: articolCategoriesList!=null?articolCategoriesList.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child:
                            Padding(
                                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                child: InkWell(
                                    onTap: () => _onTapItem(context, articolCategoriesList[index]),
                                    child:Container(
                                  height: kRowHeight40,
                                  width: MediaQuery.of(context).size.width,
                                  color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                                  child: Stack(
                                      children: <Widget>[
                                  new Align(
                                  alignment: Alignment.bottomRight,
                                      child: Image.asset(
                                        "assets/img/polygon_small.png",
                                        width: 14.0,
                                        height: 14.0,
//                                      fit: BoxFit.cover,
                                      )),
                                  Center(
                                    child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(
                                            flex: 6,
                                            child:Padding(
                                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                              child: Text(articolCategoriesList[index].articolCategoryName, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 4,
                                             child: Align(
                                                 alignment: Alignment.centerRight,
                                                 child:Padding(
                                                   padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                                   child: Text(articolCategoriesList[index].getVanzariRoundedAsString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                                 )),
                                          ),
                                          Expanded(
                                            flex: 4,
                                             child: Align(
                                                 alignment: Alignment.centerRight,
                                                 child: Padding(
                                                   padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                                   child: Text(articolCategoriesList[index].getCastigRoundedAsString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWred, fontSize: kFontSize14)),
                                                 ))
                                          )

                                        ]),
                                  )
                                  ]),
                                ))),


                      );
                    },
                  ),
                ),


                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          )):articolCategoriesList==null?
      Container(
          margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWwhite,
            strokeWidth: 1,
          )):
      Expanded(
        flex: 1,

          child: Container())
    ])));
  }
  void _onTapItem(BuildContext context, ArticolCategoryElem post) {
    print(' articolCategoryId period>> '+period.toString());
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              ValorificareDetailScreen(post, period)),
    );
  }


  Future<VanzareArticolElem> makeGetVanzareArticolCategories(BuildContext context, String basicUrl, String period) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/VanzareArticolCategories"+"?period="+period;
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' VanzareArticolCategories response elot t '+url.toString());
      print('VanzareArticolCategories  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('VanzareArticolCategories  response elott response tostring>'+response.toString());
        VanzareArticolElem vanzareArticolElem = new VanzareArticolElem();
        final responseJson = json.decode(response.body);
        vanzareArticolElem = new VanzareArticolElem.fromJson(responseJson);

        return vanzareArticolElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }


    return null;
  }


}


class VanzareArticolElem {
  final double totalVanzare;
  final double totalCastig;
  final List<ArticolCategoryElem> articolCategories;


  VanzareArticolElem({this.totalVanzare, this.totalCastig, this.articolCategories});

  factory VanzareArticolElem.fromJson(Map<String, dynamic> parsedJson) {
    var articolCategories = parsedJson['Articol_Categories']!=null?parsedJson['Articol_Categories'] as List:null;
    List<ArticolCategoryElem> articolCategoriesList = articolCategories!=null?articolCategories.map((i) => ArticolCategoryElem.fromJson(i)).toList():null;
    return VanzareArticolElem(
        totalVanzare: parsedJson['Total_Vanzare'],
        totalCastig: parsedJson['Total_Castig'],
        articolCategories: articolCategoriesList
    );
  }

}

class ArticolCategoryElem {
  final int articolCategoryId;
  final String articolCategoryName;
  final double vanzareBrut;
  final double castig;

  ArticolCategoryElem(
      {this.articolCategoryId,
        this.articolCategoryName,
        this.vanzareBrut,
        this.castig});

  factory ArticolCategoryElem.fromJson(Map<String, dynamic> parsedJson) {
    return ArticolCategoryElem(
        articolCategoryId: parsedJson['Articol_Category_Id'],
        articolCategoryName: parsedJson['Articol_Category_Name'],
        vanzareBrut: parsedJson['Vanzare_Brut'],
        castig: parsedJson['Castig']
    );
  }

  String getVanzariAsString(){
    if (vanzareBrut == null){
      return "";
    }
    return vanzareBrut.toString();
  }
  String getVanzariRoundedAsString(){
    if (vanzareBrut == null){
      return "";
    }
    return NumberFormat.currency(locale: 'eu', symbol: '').format(vanzareBrut);
  }
  String getCastigAsString(){
    if (castig == null){
      return "";
    }
    return castig.toString();
  }
  String getCastigRoundedAsString(){
    if (castig == null){
      return "";
    }
    return NumberFormat.currency(locale: 'eu', symbol: '').format(castig);
  }
}

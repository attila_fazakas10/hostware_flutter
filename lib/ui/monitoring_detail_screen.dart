import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/monitoring_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

ScrollController scrollControllerMonitoringDetail;

class MonitoringDetailScreen extends StatefulWidget {
  MonitoringMasaElem data;

  MonitoringDetailScreen(MonitoringMasaElem data_, {Key key}) : super(key: key) {
    data = data_;
  }

  @override
  MonitoringDetailState createState() => MonitoringDetailState(this.data);
}

class MonitoringDetailState extends State<MonitoringDetailScreen> {
  MonitoringMasaElem data;

  MonitoringDetailState(this.data);

  @override
  void initState() {

    scrollControllerMonitoringDetail = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    scrollControllerMonitoringDetail.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Masa "+((data!=null&& data.masa!=null)?data.masa.toString():""),
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        child: new Container(
          color: Theme.Colors.HWwhite,
          height: kRowHeight23,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    data!=null? data.ospatarName:"",
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              )
            ],
          ),
        ),
      ),

      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        color: Theme.Colors.HWrowGrey,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(),
          ),

          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Cantitate", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  ))),

          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Valoare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  )))
        ]),
      ),
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerMonitoringDetail,
                    itemExtent: kRowHeight40,
                    itemCount: (data!=null && data.articole!=null)?data.articole.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                              child: Stack(children: <Widget>[

                                Center(
                                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                        child: Text(data.articole[index].articolName, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text(data.articole[index].cantitate.toString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text(NumberFormat.currency(locale: 'eu', symbol: 'lei').format(data.articole[index].valoare), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    )
                                  ]),
                                )
                              ]),
                            )),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
    ])));
  }


}

class MyDataArticole {
  String text;
  int value;
  double value2;

  MyDataArticole({this.text, this.value, this.value2});
}

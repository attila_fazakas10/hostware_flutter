import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/ui/parteneri_screen.dart';
import 'package:hostware_flutter/ui/error_screen.dart';
import 'package:hostware_flutter/ui/parteneri_detail_more_screen.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:hostware_flutter/utils/shared_pref.dart';

import 'package:intl/intl.dart';
import 'package:date_format/date_format.dart';

ScrollController scrollControllerParteneriDetail;
Timer _timer;

class ParteneriDetailScreen extends StatefulWidget {
  PartenerElem data;
  String period;

  ParteneriDetailScreen(PartenerElem data_, String period_, {Key key}) : super(key: key) {
    data = data_;
    period = period_;
  }

  @override
  ParteneriDetailState createState() => ParteneriDetailState(this.data, this.period);
}

class ParteneriDetailState extends State<ParteneriDetailScreen> {
  PartenerElem data;
  String period;
  PartenerDetailListElem partenerDetailListElem;

  int selectedTab = 1;

  double total;
  double minusTotalNote = 0;
  double minusTotalArticole = 0;
  double minusTotalModuri = 0;
  List<double> notesWidths;
  List<double> articoleWidths;
  List<double> moduriWidths;
  double maxNotesValue;
  double maxArticoleValue;
  double maxModuriValue;

  ParteneriDetailState(this.data, this.period);

  @override
  void initState() {
    initData();

    scrollControllerParteneriDetail = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    if (_timer!=null) {
      _timer.cancel();
    }
    scrollControllerParteneriDetail.dispose();
    super.dispose();
  }

  void initData() {

    setState(() {

      total = data.value;

    });
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetParteneriDetailsList(context, url, period).then((response) {
        setState(() {
          notesWidths = new List<double>();
          articoleWidths = new List<double>();
          moduriWidths = new List<double>();
          maxNotesValue = 0;
          maxArticoleValue = 0;
          maxModuriValue = 0;

          if (response != null) {
            partenerDetailListElem = response;
            if (partenerDetailListElem != null && partenerDetailListElem.parteneri != null) {
              for (PartenerElem partenerElem in partenerDetailListElem.parteneri) {
                if (partenerElem.notes != null) {
                  for (NoteElem noteElem in partenerElem.notes) {
                    notesWidths.add(0);
                    if (noteElem.value  > maxNotesValue) {
                      maxNotesValue = noteElem.value;
                    }
                    if (noteElem.value < 0) {
                      minusTotalNote += noteElem.value;
                    }
                  }
                }
                if (partenerElem.articole != null) {
                  for (PartenerArticolElem partenerArticolElem in partenerElem.articole) {
                   articoleWidths.add(0);
                   if (partenerArticolElem.vanzareBrut  > maxArticoleValue) {
                     maxArticoleValue = partenerArticolElem.vanzareBrut;
                   }
                    if (partenerArticolElem.vanzareBrut < 0) {
                      minusTotalArticole += partenerArticolElem.vanzareBrut;
                    }
                  }
                }
                if (partenerElem.moduriDePlata != null) {
                  for (ModDePlataElem modDePlataElem in partenerElem.moduriDePlata) {
                    moduriWidths.add(0);
                    if ( modDePlataElem.value > maxModuriValue) {
                      maxModuriValue =  modDePlataElem.value;
                    }
                    if (modDePlataElem.value < 0) {
                      minusTotalModuri += modDePlataElem.value;
                    }
                  }
                }
                startTimer();
              }
            }
          } else {
            print('initstate hivas  null ');
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
    });
  }

  void startTimer() {
    _timer = Timer.periodic(Duration(milliseconds: 300), (Timer t) {
      _timer.cancel();
      setState(() {
        if (partenerDetailListElem != null && partenerDetailListElem.parteneri != null) {
          for (PartenerElem partenerElem in partenerDetailListElem.parteneri) {
            if (partenerElem.notes != null) {
              int i = 0;
              for (NoteElem noteElem in partenerElem.notes) {
                notesWidths[i] = noteElem.value;
                i++;
              }
            }
            if (partenerElem.articole != null) {
               int i = 0;
              for (PartenerArticolElem partenerArticolElem in partenerElem.articole) {
               articoleWidths[i] = partenerArticolElem.vanzareBrut;
               i++;
              }
            }
            if (partenerElem.moduriDePlata != null) {
              int i = 0;
              for (ModDePlataElem modDePlataElem in partenerElem.moduriDePlata) {
                moduriWidths[i] = modDePlataElem.value;
                i++;
              }
            }
          }
        }


      });
    });
  }
  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {

          partenerDetailListElem = new PartenerDetailListElem();
        });

      }
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    data.partnerName,
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
              Container(
                height: 22,
                color: Theme.Colors.HWlightestGrey,
                child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                        onTap: () => _onTapOrderVanzari(1),
                        child:Container(
                      margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 1.0, right: 0.0),
                      color: (selectedTab==1)?Theme.Colors.HWlightestGrey:Theme.Colors.HWwhite,
                        child: Align(
                         alignment: Alignment.center,
                         child: Padding(
                           padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                           child: Text("Note de plata", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                         )),
                    ))
                  ),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => _onTapOrderVanzari(2),
                          child:Container(
                        color: (selectedTab==2)?Theme.Colors.HWlightestGrey:Theme.Colors.HWwhite,
                        margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 1.0, right: 1.0),
                        child: Align(
                           alignment: Alignment.center,
                           child: Padding(
                             padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                             child: Text("Articole", style: TextStyle(fontWeight: FontWeight.normal, color:Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                           )),
                      ))),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => _onTapOrderVanzari(3),
                          child:Container(
                            color: (selectedTab==3)?Theme.Colors.HWlightestGrey:Theme.Colors.HWwhite,
                            margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 0.0, right: 1.0),
                            child: Align(
                                alignment: Alignment.center,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                                  child: Text("Moduri de plata", style: TextStyle(fontWeight: FontWeight.normal, color:Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                                )),
                          )))
                ]),
              ),
              Container(
                height: kMargin20,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWwhite,
              ),
      Container(
        height: kRowHeight23,
        child: new Container(
          color: Theme.Colors.HWwhite,
          height: kRowHeight23,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Total",
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin20),
                child: Container(
                  child: Text(
                      NumberFormat.currency(locale: 'eu', symbol: 'lei').format(total),
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        color: Theme.Colors.HWrowGrey,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Expanded(
            flex: 10,
            child: Container(),
          ),

          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Valoare", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  )))
        ]),
      ),
      partenerDetailListElem!=null?
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerParteneriDetail,
                    itemExtent: kRowHeight40,
                    itemCount: (selectedTab==1&&partenerDetailListElem!=null && partenerDetailListElem.parteneri!=null &&  partenerDetailListElem.parteneri.length > 0 && partenerDetailListElem.parteneri[0].notes!=null)?partenerDetailListElem.parteneri[0].notes.length:
                    (selectedTab==2&&partenerDetailListElem!=null && partenerDetailListElem.parteneri!=null &&  partenerDetailListElem.parteneri.length > 0 && partenerDetailListElem.parteneri[0].articole!=null)?partenerDetailListElem.parteneri[0].articole.length:
                    (selectedTab==3&&partenerDetailListElem!=null && partenerDetailListElem.parteneri!=null &&  partenerDetailListElem.parteneri.length > 0 && partenerDetailListElem.parteneri[0].moduriDePlata!=null)?partenerDetailListElem.parteneri[0].moduriDePlata.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: InkWell(
                      onTap: () => _onTapItem(context, selectedTab == 1?partenerDetailListElem.parteneri[0].notes[index]:null, data.partnerId, data.partnerName),
                      child:Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                              child: Stack(children: <Widget>[
                                new Align(
                                    alignment: Alignment.bottomRight,
                                    child: Image.asset(
                                      "assets/img/polygon_small.png",
                                      width: (selectedTab==1)?14.0:0.0,
                                      height: 14.0,
//                                      fit: BoxFit.cover,
                                    )),
                                new Align(
                                    alignment: Alignment.bottomLeft,
                                    child: AnimatedContainer(
                                      curve: Curves.ease,
                                      duration: new Duration(milliseconds: 800),
                                      decoration: BoxDecoration(
                                          color: Theme.Colors.HWred,
                                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                          border: Border.all(
                                            color: Theme.Colors.HWred,
                                            width: 1,
                                          )),
                                      height: 5,
                                      //ez azert ilyen bonyolult, mert negativ ertekek is lehetnek, es azokat az aranyszamolasnal nem veszem figyelembe, hanem 0 lesz a width
//                                      width: (MediaQuery.of(context).size.width*(selectedTab==1?notesWidths[index]:selectedTab==2?articoleWidths[index]:selectedTab==3?moduriWidths[index]:0))/((selectedTab==1?maxNotesValue:selectedTab==2?maxArticoleValue:maxModuriValue))>0?
//                                      (MediaQuery.of(context).size.width*(selectedTab==1?notesWidths[index]:selectedTab==2?articoleWidths[index]:selectedTab==3?moduriWidths[index]:0))/((selectedTab==1?maxNotesValue:selectedTab==2?maxArticoleValue:maxModuriValue)):0,
                                      width: (selectedTab==1?maxNotesValue:selectedTab==2?maxArticoleValue:maxModuriValue)>0? ((MediaQuery.of(context).size.width*(selectedTab==1?notesWidths[index]:selectedTab==2?articoleWidths[index]:selectedTab==3?moduriWidths[index]:0))/((selectedTab==1?maxNotesValue:selectedTab==2?maxArticoleValue:maxModuriValue))):0,
                                    )),
                                Center(
                                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                    Expanded(
                                      flex: 10,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                        child: Text(selectedTab==1?convertDateFromString(partenerDetailListElem.parteneri[0].notes[index].timestamp):selectedTab==2?partenerDetailListElem.parteneri[0].articole[index].articolName:selectedTab==3?partenerDetailListElem.parteneri[0].moduriDePlata[index].title:"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text(selectedTab==1? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(partenerDetailListElem.parteneri[0].notes[index].value):selectedTab==2? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(partenerDetailListElem.parteneri[0].articole[index].vanzareBrut):selectedTab==3? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(partenerDetailListElem.parteneri[0].moduriDePlata[index].value):"", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    )
                                  ]),
                                )
                              ]),
                            ))),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          ))
          :
      Container(
          margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWwhite,
            strokeWidth: 1,
          ))
    ])));
  }

  String convertDateFromString(String strDate){
    DateFormat inputFormat = DateFormat("dd/MM/yyyy hh:mm:ss a");
    DateTime dateTime = inputFormat.parse(strDate);
    print('DATE:::::'+formatDate(dateTime, [yyyy, '/', mm, '/', dd, ' ', hh, ':', nn, ':', ss]));
    return formatDate(dateTime, [yyyy, '-', mm, '-', dd,' ',  hh, ':', nn]);
  }

  void _onTapOrderVanzari(int tabSel) {

    setState(() {
      if (tabSel == 1) {




      } else if (tabSel == 2){


      } else if (tabSel == 3){




      }
      selectedTab = tabSel;

    });
  }


  void _onTapItem(BuildContext context, NoteElem noteElem, int partnerId, String partnerName) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              ParteneriDetailMoreScreen(noteElem,partnerId, partnerName)),
    );
  }

  Future<PartenerDetailListElem> makeGetParteneriDetailsList(BuildContext context,String  basicUrl, String period) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/PartnerDetails"+"?period="+period+"&partnerID="+data.partnerId.toString();
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' makeGetParteneriDetailsList response elot t '+url.toString());
      print('makeGetParteneriDetailsList  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('makeGetParteneriDetailsList  response elott response tostring>'+response.toString());
        PartenerDetailListElem partenerDetailListElem = new PartenerDetailListElem();
        final responseJson = json.decode(response.body);
        partenerDetailListElem = new PartenerDetailListElem.fromJson(responseJson);

        return partenerDetailListElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }


    return null;
  }

}

class MyData1 {
  String text;
  double value;

  MyData1({this.text, this.value});
}

class PartenerDetailListElem {
  final List<PartenerElem> parteneri;

  PartenerDetailListElem({
    this.parteneri,
  });

  factory PartenerDetailListElem.fromJson(List<dynamic> parsedJson) {

    List<PartenerElem> parteneri = new List<PartenerElem>();
    parteneri = parsedJson.map((i)=>PartenerElem.fromJson(i)).toList();

    return new PartenerDetailListElem(
        parteneri: parteneri
    );
  }
}

import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/utils/constants.dart';
import 'package:hostware_flutter/utils/shared_pref.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:hostware_flutter/ui/error_screen.dart';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

ScrollController scrollControllerRulajMese;
Timer _timer;
class RulajMeseScreen extends StatefulWidget {
  String period;
  String unitate;
  RulajMeseScreen( String period_,String unitate_,{Key key}) : super(key: key) {
    period = period_;
    unitate = unitate_;
  }

  @override
  RulajMeseState createState() => RulajMeseState(this.period, this.unitate);
}

class RulajMeseState extends State<RulajMeseScreen> {
  String period;
  String unitate;
  bool isClientiOrdering = true;
  RulajMeseElem rulajMeseElem;
  List<MasaElem> meseList;
  int totalClientiValue;
  int maxClientiValue;
  String totalClientiString;
  double totalIntrariValue;
  double maxIntrariValue;
  String totalIntrariString;
  List<double> intrariWidths;
  List<double> clientiWidths;
  RulajMeseState(this.period, this.unitate);


  @override
  void initState() {
    initData();

    scrollControllerRulajMese = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    if (_timer!=null) {
      _timer.cancel();
    }
    scrollControllerRulajMese.dispose();
    super.dispose();
  }

  void initData() {
    SharedPreferencesHelper.getBasicURL().then((url) {
      makeGetRulajMese(context, url, period, unitate).then((response) {
        setState(() {
          if (response != null) {
            rulajMeseElem = response;

            totalClientiString = response.totalClienti != null ? NumberFormat.currency(locale: 'eu', symbol: '').format(response.totalClienti) + "" : "-";
            if (totalClientiString.contains(",")) {
              totalClientiString = totalClientiString.split(",")[0];
            }
            totalIntrariString = response.totalIntrari != null ? NumberFormat.currency(locale: 'eu', symbol: 'lei').format(response.totalIntrari) : "-";
            totalClientiValue = response.totalClienti;
            totalIntrariValue = response.totalIntrari;

            meseList = response.mese;
            if (meseList==null){
              meseList = new List<MasaElem>();
            }
            meseList.sort((a, b) => a.nrClienti.compareTo(b.nrClienti));
            meseList = new List.from(meseList.reversed);

            maxClientiValue = 0;
            maxIntrariValue = 0;

            intrariWidths = new List<double>();
            clientiWidths = new List<double>();
            for ( MasaElem masaElem in meseList) {
              intrariWidths.add(0);
              clientiWidths.add(0);
              if (masaElem.nrClienti > maxClientiValue) {
                maxClientiValue = masaElem.nrClienti;
              }
              if (masaElem.intrari > maxIntrariValue) {
                maxIntrariValue = masaElem.intrari;
              }
            }
            startTimer();
          } else {
            print('initstate hivas  null ');
          }
        });
      }).catchError((e) {
        print("===== ERROR: ${e.toString()}");
        this.callbackErr(e.toString());
      });
  });
  }

  void startTimer() {
    _timer = Timer.periodic(Duration(milliseconds: 300), (Timer t) {
      _timer.cancel();
      setState(() {
        int i = 0;
        print('==========maxIntrariValue:'+maxIntrariValue.toString());
        meseList.sort((a, b) => a.nrClienti.compareTo(b.nrClienti));
        meseList = new List.from(meseList.reversed);
        for ( MasaElem masaElem in meseList) {
          print('==========maxIntrariValue:'+masaElem.intrari.toString());
          clientiWidths[i] = (MediaQuery.of(context).size.width*(masaElem.nrClienti))/(maxClientiValue);
          i++;
        }
        List<MasaElem> meseList2 =  new List.from(meseList);
        meseList2.sort((a, b) => a.intrari.compareTo(b.intrari));
        meseList2 = new List.from(meseList2.reversed);
        i = 0;
        for ( MasaElem masaElem in meseList2) {
          print('==========maxIntrariValue:'+masaElem.intrari.toString());
          intrariWidths[i] = (MediaQuery.of(context).size.width*(masaElem.intrari))/(maxIntrariValue);
          i++;
        }
      });
    });
  }
  void callbackErr(String err) {
    _displayErrorScreen(context, err);

  }
  _displayErrorScreen(BuildContext context, String err) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ErrorScreen(err)),
    ).then((value) {
      if (value!=null && value.toString().contains('retry')) {
        setState(() {
//        if (selectedUnitateElem!=null) {
          print('==========Error oldalrol visszaterve');
//          callbackMakeNulls();
          initData();

//        }
        });
      } else {
        setState(() {
          totalIntrariString = "";
          totalClientiString = "";
          meseList = new List<MasaElem>();
        });

      }
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.blue, // navigation bar color
      statusBarColor: Theme.Colors.HWbarGrey, // status bar color
    ));
    return new Scaffold(
        body: SafeArea(
            child: Column(children: <Widget>[
      Container(
        height: kBarHeight,
        color: Theme.Colors.HWwhite,
        child: new Container(
          color: Theme.Colors.HWbarGrey,
          height: kBarHeight,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    "Rulaj mese",
                    style: TextStyle(color: Theme.Colors.HWwhite, fontWeight: FontWeight.normal, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    height: kBarHeight,
                    width: kBarHeight,
                    child: new IconButton(
                      icon: new Icon(Icons.close, color: Theme.Colors.HWwhite),
                      onPressed: () => Navigator.pop(context, true),
                    )),
              ),
            ],
          ),
        ),
      ),
      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
              Container(
                height: 22,
                color: Theme.Colors.lightestGrey,
                child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: InkWell(
                        onTap: () => _onTapOrderClienti(true),
                        child:Container(
                      margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 1.0, right: 0.0),
                      color: isClientiOrdering?Theme.Colors.HWrowGrey:Theme.Colors.HWwhite,
                        child: Align(
                         alignment: Alignment.center,
                         child: Padding(
                           padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                           child: Text("Nr. clienti", style: TextStyle(fontWeight: FontWeight.normal, color: isClientiOrdering?Theme.Colors.HWwhite:Theme.Colors.HWBlackTextColor, fontSize: kFontSize12)),
                         )),
                    ))
                  ),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () => _onTapOrderClienti(false),
                          child:Container(
                        color: isClientiOrdering?Theme.Colors.HWwhite:Theme.Colors.HWrowGrey,
                        margin: EdgeInsets.only(top: 1.0, bottom: 1.0, left: 0.0, right: 1.0),
                        child: Align(
                           alignment: Alignment.center,
                           child: Padding(
                             padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                             child: Text("Intrari", style: TextStyle(fontWeight: FontWeight.normal, color: isClientiOrdering?Theme.Colors.HWBlackTextColor:Theme.Colors.HWwhite, fontSize: kFontSize12)),
                           )),
                      )))
                ]),
              ),
              Container(
                height: kMargin20,
                width: MediaQuery.of(context).size.width,
                color: Theme.Colors.HWwhite,
              ),
      Container(
        height: kRowHeight23,
        child: new Container(
          color: Theme.Colors.HWwhite,
          height: kRowHeight23,
          width: MediaQuery.of(context).size.width,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: kMargin20),
                child: Container(
                  child: Text(
                    isClientiOrdering?"Total clienti":"Total intrari",
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin20),
                child: Container(
                  child: isClientiOrdering&&totalClientiString==null?
                  Container(
                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                      width: kProgressSize,
                      height: kProgressSize,
                      child: CircularProgressIndicator(
                        backgroundColor: Theme.Colors.HWwhite,
                        strokeWidth: 1,
                      )): !isClientiOrdering&&totalIntrariString==null?
                  Container(
                      margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
                      width: kProgressSize,
                      height: kProgressSize,
                      child: CircularProgressIndicator(
                        backgroundColor: Theme.Colors.HWwhite,
                        strokeWidth: 1,
                      )):
                  Text(
                    isClientiOrdering?totalClientiString:totalIntrariString,
                    style: TextStyle(color: Theme.Colors.HWBlackTextColor, fontWeight: FontWeight.normal, fontSize: kFontSize24),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      Container(
        height: kMargin20,
        width: MediaQuery.of(context).size.width,
        color: Theme.Colors.HWwhite,
      ),
      Container(
        height: kRowHeight23,
        color: Theme.Colors.HWrowGrey,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(),
          ),
          Expanded(
            flex: 4,
            child: Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                  child: Text("Clienti", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                )),
          ),
          Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                    child: Text("Intrari", style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: kFontSize12)),
                  )))
        ]),
      ),
      meseList!=null?
      Expanded(
          flex: 1,
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: new ListView.builder(
                    padding: EdgeInsets.all(0.0),
                    controller: scrollControllerRulajMese,
                    itemExtent: kRowHeight40,
                    itemCount: meseList!=null?meseList.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        /*decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        border: Border.all(color: Colors.black),
                      ),*/

                        child: Padding(
                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: 0.0),
                            child: Container(
                              height: kRowHeight40,
                              width: MediaQuery.of(context).size.width,
                              color: (index % 2 == 0) ? Theme.Colors.HWwhite : Theme.Colors.HWgreyLightest,
                              child: Stack(children: <Widget>[
                                new Align(
                                    alignment: Alignment.bottomLeft,
                                    child: AnimatedContainer(
                                      curve: Curves.ease,
                                      duration: new Duration(milliseconds: 800),
                                      decoration: BoxDecoration(
                                          color: isClientiOrdering?Theme.Colors.HWBlackTextColor:Theme.Colors.HWred,
                                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                                          border: Border.all(
                                            color: isClientiOrdering?Theme.Colors.HWBlackTextColor:Theme.Colors.HWred,
                                            width: 1,
                                          )),
                                      height: 5,
                                      width: isClientiOrdering&&maxClientiValue>0?clientiWidths[index]:
                                        !isClientiOrdering&&maxIntrariValue>0?intrariWidths[index]:0,
                                    )),
                                Center(
                                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: kMargin10, right: 0.0),
                                        child: Text("Masa "+meseList[index].masaName, style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                            child: Text(meseList[index].nrClienti.toString(), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWBlackTextColor, fontSize: kFontSize14)),
                                          )),
                                    ),
                                    Expanded(
                                        flex: 4,
                                        child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Padding(
                                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right: kMargin10),
                                              child: Text(NumberFormat.currency(locale: 'eu', symbol: 'lei').format(meseList[index].intrari), style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWred, fontSize: kFontSize14)),
                                            )))
                                  ]),
                                )
                              ]),
                            )),
                      );
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(top: 0.0, bottom: 20.0, left: kMargin10, right: kMargin10),
                    child: Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Theme.Colors.HWwhite,
                    )),
              ],
            ),
          )):
      Container(
          margin: EdgeInsets.only(top: kMargin4, bottom: kMargin4, left: 0.0, right: 0.0),
          width: kProgressSize,
          height: kProgressSize,
          child: CircularProgressIndicator(
            backgroundColor: Theme.Colors.HWwhite,
            strokeWidth: 1,
          ))
    ])));
  }

  void _onTapOrderClienti(bool clientiClicked) {

    setState(() {
      if (clientiClicked) {
        meseList.sort((a, b) => a.nrClienti.compareTo(b.nrClienti));
        isClientiOrdering = true;
      } else {
        meseList.sort((a, b) => a.intrari.compareTo(b.intrari));
        isClientiOrdering = false;
      }
      meseList = new List.from(meseList.reversed);

    });
  }


  Future<RulajMeseElem> makeGetRulajMese(BuildContext context, String basicUrl, String period, String unitate) async {
    try {
      var headers = {
        'Accept': 'application/json'
      };
      var url =
          basicUrl+"/RulajMese"+"?period="+period+"&unitate="+unitate;
      var response = await http.get(url, headers: headers)
          .timeout(const Duration(seconds: 30));
      print(' makeGetRulajMese response elot t '+url.toString());
      print('makeGetRulajMese  response elott 2 '+response.statusCode.toString());
      if (response.statusCode == 200) {
        print('makeGetRulajMese  response elott response tostring>'+response.toString());
        RulajMeseElem rulajMeseElem = new RulajMeseElem();
        final responseJson = json.decode(response.body);
        rulajMeseElem = new RulajMeseElem.fromJson(responseJson);

        return rulajMeseElem;
      } else {
        throw Exception('Failed to load internet');
      }
    } on TimeoutException catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } on Exception catch (_) {
      throw Exception(_.toString());
      // A timeout occurred.
    } catch (exception){
      throw Exception(exception.toString());
    }



    return null;
  }


}

class RulajMeseElem{
  final int totalClienti;
  final double totalIntrari;
  final List<MasaElem> mese;

  RulajMeseElem({
    this.totalClienti,
    this.totalIntrari,
    this.mese
  }) ;

  factory RulajMeseElem.fromJson(Map<String, dynamic> parsedJson){
    var mese = parsedJson['mese']!=null?  parsedJson['mese'] as List:null;
    List<MasaElem> meseList = mese!=null?mese.map((i) => MasaElem.fromJson(i)).toList():null;
    return new RulajMeseElem(
        totalClienti: parsedJson['total_clienti'],
        totalIntrari: parsedJson['total_intrari'],
        mese: meseList
    );
  }

}

class MasaElem{
  final String masaName;
  final int nrClienti;
  final double intrari;

  MasaElem({
    this.masaName,
    this.nrClienti,
    this.intrari
  }) ;

  factory MasaElem.fromJson(Map<String, dynamic> parsedJson){
    return new MasaElem(
        masaName: parsedJson['masa_name'],
        nrClienti: parsedJson['nr_clienti'],
        intrari: parsedJson['intrari']
    );
  }

}
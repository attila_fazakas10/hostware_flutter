import 'dart:ui';

import 'package:flutter/cupertino.dart';

class Colors {

  const Colors();

  static const Color HWbarGrey = const Color(0xFF3A444F);
  static const Color HWrowGrey = const Color(0xFF586878);
  static const Color HWlightGrey = const Color(0xFFC4C4C4);
  static const Color HWlightestGrey = const Color(0xFFF3F3F3);
  static const Color HWred = const Color(0xFFB31A41);
  static const Color HWwhite = const Color(0xFFFFFFFF);
  static const Color HWgreyLightest = const Color(0xFFF3F3F3);
  static const Color HWyellow = const Color(0xFFEFBB2B);
  static const Color HWTextColor = const Color(0xFF6F6E6E);
  static const Color HWBlackTextColor = const Color(0xFF030303);
  static const Color HWGreyTextColor = const Color(0xFF6F6E6E);
  static const Color HWGreyBorderColor = const Color(0xFFE5E5E5);
  static const Color HWSwitchColor = const Color(0xFFBCC3C9);

  static const Color HWPaletteRed1 = const Color(0xFFDD403A);
  static const Color HWPaletteRed2 = const Color(0xFFAB4A7F);
  static const Color HWPaletteOrange1 = const Color(0xFFDB8585);
  static const Color HWPaletteOrange2 = const Color(0xFFF4A261);
  static const Color HWPaletteGreen1 = const Color(0xFF219653);
  static const Color HWPaletteGreen2 = const Color(0xFF6AB9A1);
  static const Color HWPaletteGreen3 = const Color(0xFFA89536);
  static const Color HWPaletteBlue1 = const Color(0xFF40D1DB);
  static const Color HWPaletteBlue2 = const Color(0xFF00499E);
  static const Color HWPaletteBlue3 = const Color(0xFF09334C);

  static const Color loginGradientStart = const Color(0xFFffffff);
  static const Color loginGradientEnd = const Color(0xFFffffff);
  static const Color optimoo_white = const Color(0xFFffffff);
  static const Color optimoo_semiwhite = const Color(0x55f1f1f1);
  static const Color optimoo_whitetotal = const Color(0xFFffffff);
  static const Color optimoo_semitrans = const Color(0xbb000000);
  static const Color optimoo_semitranswhite = const Color(0xbb000000);
  static const Color optimoo_blue = const Color(0xFF428dff);
  static const Color loginDarkGreen = const Color(0xFF54cc68);


  static const Color loginGreen = const Color(0xFF54cc68);
  static const Color loginGreenEnd = const Color(0xFF54cc68);
  static const Color loginGrey = const Color(0xFFb9b9b9);
  static const Color starFull = const Color(0xFFfcd12a);
  static const Color starEmpty = const Color(0x99b9b9b9);
  static const Color loginDarkGrey = const Color(0xFF252525);
  static const Color darkGrey = const Color(0xFF757575);
  static const Color darkest = const Color(0xFF000000);
  static const Color loginLightGrey = const Color(0x99b9b9b9);
  static const Color lightestGrey = const Color(0xFFb9b9b9);
  static const Color transparent = const Color(0x00000000);

  static const Color atWorkColor = const Color(0xFF9ac3f7);
  static const Color missingColor = const Color(0xFF99edcc);
  static const Color latesColor = const Color(0xFFcb948d);
  static const Color breakColor = const Color(0xFFe36688);
  static const Color leaveColor = const Color(0xFFfcd12a);
  static const Color businessColor = const Color(0xFF40826d);
  static const Color holidayColor = const Color(0xFF99275a);
  static const Color medicalHolidayColor = const Color(0xFFcc275a);
  static const Color travelColor = const Color(0xFFff7e6b);

  static const primaryGradient = const LinearGradient(
    colors: const [loginGradientStart, loginGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/services.dart';
import 'package:hostware_flutter/style/theme.dart' as Theme;
import 'package:hostware_flutter/ui/home_screen.dart';
import 'package:hostware_flutter/utils/constants.dart';
import 'dart:async';

Timer _timer;
int _start = 3;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Theme.Colors.HWPaletteBlue1, // navigation bar color
      statusBarColor: new prefix0.Color(0xFF3A444F), // status bar color
    ));
    return MaterialApp(
      title: 'HW Watchdog',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.

//        primarySwatch: Colors.grey,
        tabBarTheme: TabBarTheme(
          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
          labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),

          /// comment this out to make it work
        ),
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _startHome() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body:
          Stack(
            children: <Widget>[
          Container(
          color: Theme.Colors.HWbarGrey,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,

            child: Column(
              // Column is also layout widget. It takes a list of children and
              // arranges them vertically. By default, it sizes itself to fit its
              // children horizontally, and tries to be as tall as its parent.
              //
              // Invoke "debug painting" (press "p" in the console, choose the
              // "Toggle Debug Paint" action from the Flutter Inspector in Android
              // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
              // to see the wireframe for each widget.
              //
              // Column has various properties to control how it sizes itself and
              // how it positions its children. Here we use mainAxisAlignment to
              // center the children vertically; the main axis here is the vertical
              // axis because Columns are vertical (the cross axis would be
              // horizontal).
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child:  new Image(image: new AssetImage("assets/logo_big.png"),fit: BoxFit.fill)
                ),
                Expanded(
                  flex:1,
                  child: Container(),
                )
              ],
            ),
            // This trailing comma makes auto-formatting nicer for build methods.
          ),
              Container(
                color: Theme.Colors.transparent,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,

                child: Column(
                  // Column is also layout widget. It takes a list of children and
                  // arranges them vertically. By default, it sizes itself to fit its
                  // children horizontally, and tries to be as tall as its parent.
                  //
                  // Invoke "debug painting" (press "p" in the console, choose the
                  // "Toggle Debug Paint" action from the Flutter Inspector in Android
                  // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
                  // to see the wireframe for each widget.
                  //
                  // Column has various properties to control how it sizes itself and
                  // how it positions its children. Here we use mainAxisAlignment to
                  // center the children vertically; the main axis here is the vertical
                  // axis because Columns are vertical (the cross axis would be
                  // horizontal).
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        color: Theme.Colors.transparent,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.bottomRight,
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.only(top: 0.0, bottom: 10.0, left: 0.0, right:kMargin20),
                        child: Text('HW', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: 48)),
                      )
                    )
                    ,
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                              alignment: Alignment.topRight,
                            child:Padding(
                              padding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 0.0, right:kMargin20),
                              child: Text('WATCHDOG', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWwhite, fontSize: 48)),
                            ),
                          ),
                          Container(
                            alignment: Alignment.bottomCenter,
                            padding: EdgeInsets.only(top: 0.0, bottom: 50.0, left: 0.0, right:0.0),

                            child: Center(child: Text('by Vivid Systems', style: TextStyle(fontWeight: FontWeight.normal, color: Theme.Colors.HWSwitchColor, fontSize: kFontSize24))),

                          ),
                        ],
                      ),
                    )

                  ],
                ),
                // This trailing comma makes auto-formatting nicer for build methods.
              )
            ]
          ),
          );
  }
  @override
  void initState() {
    startTimer();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.initState();

  }
  void startTimer() {
    print('111111 HW splash ' + _start.toString());
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) {
      if (_start < 1) {
        print('HW splash ' + _start.toString());
        _timer.cancel();
        _startHome();
      } else {
        print('HW splash ' + _start.toString());
        _start = _start - 1;
      };
    });
  }

}

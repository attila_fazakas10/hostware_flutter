import 'package:flutter/painting.dart';

//const String BASIC_URL = "http://chimtarga.asuscomm.com:880/api/FireBird";
const String BASIC_URL = "http://86.123.108.184:859/api/FireBird";
const String BASIC_PATH = "/api/FireBird";
const String APPLICATION_JSON = "application/json";



const String ATWORK_STATUS_CONST = "Prezent";
const String MISSING_STATUS_CONST = "Absent";
const String BREAK_STATUS_CONST = "In pauza";
const String LEAVE_STATUS_CONST = "A plecat";
const String BUSINESS_STATUS_CONST = "Interes de serviciu";
const String HOLIDAY_REGULAR_STATUS_CONST = "Concediu de Odihn";
const String HOLIDAY_PAID_STATUS_CONST = "Concediu pl";
const String HOLIDAY_MEDICAL_STATUS_CONST = "Concediu medical";
const String TRAVEL_STATUS_CONST = "Delega";
const String VERSION = "1.2.1";

const String BTN_CANCEL_TEXT = "Cancel";

const EdgeInsets kTabLabelPadding = EdgeInsets.symmetric(horizontal: 12.0);

/// The padding added around material list items.
const EdgeInsets kMaterialListPadding = EdgeInsets.symmetric(vertical: 8.0);


const double kFontSize12 = 12.0;
const double kFontSize14 = 14.0;
const double kFontSize24 = 20.0;
const double kFontSize36 = 26.0;

const double kMargin4 = 4.0;
const double kMargin10 = 10.0;
const double kMargin20 = 20.0;
const double kMargin30 = 30.0;
const double kBarHeight = 60.0;
const double kRowHeight68 = 68.0;
const double kRowHeight60 = 60.0;
const double kRowHeight40 = 40.0;
const double kRowHeight35 = 35.0;
const double kRowHeight80 = 90.0;
const double kRowHeight23 = 27.0;
const double kRowHeight30 = 30.0;
const double kProgressSize = 16.0;

const double kBottomBarIconSize = 24.0;
/*

--------------------------------------
/Incasari/{period}/{unitate}
--------------------------------------

{
  "rulaj_brut" : 39847.87,
  "majorari" : 98.20,
  "reducere_tichete" : -1198.20,
  "incasari" : 2342398.20,
  "nr_note" : 1150,
  "nr_oaspeti" : 98.20
}


--------------------------------------
/ReducereTichete/{period}/{unitate}
--------------------------------------

[
  {
    "title":"Discount 10% alcoolice",
    "value":-2129.89
  },
  {
    "title":"Discount 10% bucatarie",
    "value":-2129.89
  },
  {
    "title":"Discount 10% nealcoolice",
    "value":-2129.89
  },
  {
    "title":"Discount 10% bucatarie",
    "value":-22129.89
  }

]


--------------------------------------
/IncasariDetails/{period}/{unitate}
--------------------------------------

[
  {
    "payment_type":"Numerar",
    "value": 3499378.98
  },
  {
    "payment_type":"Card",
    "value": 2129.89
  },
  {
    "payment_type":"Virament",
    "value": 2129.89
  },
  {
    "payment_type":"Protocol",
    "value": 22129.89
  }

]


--------------------------------------
/Stornari/{period}/{unitate}
--------------------------------------

[
  {
    "title":"Storno articole",
    "value":-2129.89
  },
  {
    "title":"Storno reduceri",
    "value": 2129.89
  },
  {
    "title":"Storno majorari",
    "value":2129.89
  },
  {
    "title":"Storno TIPS",
    "value": 0.00
  }
]



--------------------------------------
/Articole/{period}/{unitate}
--------------------------------------
{
  "total_vanzare" : 1234.54,
  "total_castig" : 342.56,
  "articol_categories": [
    {
    "articol_category_id": 1,
    "articol_category_name":"Fast food",
    "vanzare_brut": 2129.89,
    "castig": 29.89
    },
    {
    "articol_category_id": 2,
    "articol_category_name":"Good morning/Breakfast",
    "vanzare_brut": 219.89,
    "castig": 12.89
    },
    {
    "articol_category_id": 3,
    "articol_category_name":"Healthy Dishes",
    "vanzare_brut": 2129.89,
    "castig": 129.89
    }

  ]
}


--------------------------------------
/ArticoleByCategory/{period}/{unitate}/{categoryId}
--------------------------------------
{
  "total_vanzare" : 1234.54,
  "total_castig" : 342.56,
  "articole": [
            {
              "articol_name":"Articol 1",
              "vanzare_brut": 2129.89,
              "castig": 29.89
            },
            {
              "articol_name":"Articol 2",
              "vanzare_brut": 219.89,
              "castig": 12.89
            },
            {
              "articol_name":"Articol 3",
              "vanzare_brut": 2129.89,
              "castig": 129.89
            }
          ]
}


--------------------------------------
/RulajMese/{period}/{unitate}
--------------------------------------
{
  "total_clienti" : 1234,
  "total_intrari" : 1342.56,
  "mese": [
      {
        "masa_name":"Masa 1",
        "nr_clienti": 2,
        "intrari": 29.89
      },
      {
        "masa_name":"Masa 2",
        "nr_clienti": 123,
        "intrari": 129.89
      },
      {
        "masa_name":"Masa 1",
        "nr_clienti": 32,
        "intrari": 1129.89
      }
    ]
}
--------------------------------------
/CentralizatorIncasari/{period}/{unitate}
--------------------------------------
[
  {
    "ospatar_id": 1,
    "ospatar_name":"Alexandra",
    "total": 4129.89,
    "payments" : [
              {
                "payment_type_id" : 1,
                "payment_type_name" : "Numerar",
                "payment_value" : 1243.54
              },
              {
                "payment_type_id" : 2,
                "payment_type_name" : "Card",
                "payment_value" : 1243.54
              },
              {
                "payment_type_id" : 3,
                "payment_type_name" : "Virament",
                "payment_value" : 1243.54
              },
              {
                "payment_type_id" : 4,
                "payment_type_name" : "Protocol",
                "payment_value" : 1243.54
              },
          ]
  },
  {
    "ospatar_id": 2,
    "ospatar_name":"Ale Ionela",
    "total": 3241.89,
    "payments" : [
        {
          "payment_type_id" : 1,
          "payment_type_name" : "Numerar",
          "payment_value" : 1243.54
        },
        {
          "payment_type_id" : 2,
          "payment_type_name" : "Card",
          "payment_value" : 1243.54
        },
        {
          "payment_type_id" : 3,
          "payment_type_name" : "Virament",
          "payment_value" : 1243.54
        },
        {
          "payment_type_id" : 4,
          "payment_type_name" : "Protocol",
          "payment_value" : 1243.54
        },
    ]
  }
]


--------------------------------------
/IncasariOspatar/{period}/{unitate}/{ospatarId}
--------------------------------------

{
    "ospatar_name":"Alexandra",
    "payments_total" : [
      {
        "payment_type_id" : 1,
        "payment_type_name" : "Numerar",
        "payment_value" : 33243.54
      },
      {
        "payment_type_id" : 2,
        "payment_type_name" : "Card",
        "payment_value" : 343.54
      },
      {
        "payment_type_id" : 3,
        "payment_type_name" : "Virament",
        "payment_value" : 3443.54
      },
      {
        "payment_type_id" : 4,
        "payment_type_name" : "Protocol",
        "payment_value" : 33.54
      }
    ],
  "payments" : [
    {
      "date" : "11.05.2018",
      "payments_day" : [
        {
          "payment_type_id" : 1,
          "payment_type_name" : "Numerar",
          "payment_value" : 1243.54
        },
        {
          "payment_type_id" : 2,
          "payment_type_name" : "Card",
          "payment_value" : 43.54
        },
        {
          "payment_type_id" : 3,
          "payment_type_name" : "Virament",
          "payment_value" : 443.54
        },
        {
          "payment_type_id" : 4,
          "payment_type_name" : "Protocol",
          "payment_value" : 3.54
        }
      ]

    },
    {
      "date" : "12.06.2018",
      "payments_day" : [
        {
          "payment_type_id" : 1,
          "payment_type_name" : "Numerar",
          "payment_value" : 1243.54
        },
        {
          "payment_type_id" : 2,
          "payment_type_name" : "Card",
          "payment_value" : 43.54
        },
        {
          "payment_type_id" : 3,
          "payment_type_name" : "Virament",
          "payment_value" : 443.54
        },
        {
          "payment_type_id" : 4,
          "payment_type_name" : "Protocol",
          "payment_value" : 3.54
        }
      ]

    },

  ]
}

--------------------------------------
/Partners/{period}/{unitate}
--------------------------------------

[
  {
    "partner_id" : 1,
    "partner_name" : "Nume partner1",
    "nr_oaspeti" : 124,
    "value" : 23243.54
  },
  {
    "partner_id" : 2,
    "partner_name" : "Nume partner2",
    "nr_oaspeti" : 124,
    "value" : 1243.54
  },
  {
    "partner_id" : 3,
    "partner_name" : "Nume partner3",
    "nr_oaspeti" : 124,
    "value" : 43.54
  },
  {
    "partner_id" : 4,
    "partner_name" : "Nume partner4",
    "nr_oaspeti" : 124,
    "value" : 243.54
  }

]



--------------------------------
/PartnerDetails/{period}/{unitate}/{partnerId}
--------------------------------------

{
  "partner_name" : "Nume partener",
  "total" : 23983.94,
  "note" : [
    {
      "timestamp" : 12121367, //vagy time:"2019-12-12 12:12"
      "value" : 123.54
    },
    {
      "timestamp" : 12121367, //vagy time:"2019-12-12 12:12"
      "value" : 123.54
    },
    {
      "timestamp" : 12121367, //vagy time:"2019-12-12 12:12"
      "value" : 123.54
    },
    {
      "timestamp" : 12121367, //vagy time:"2019-12-12 12:12"
      "value" : 123.54
    }

  ],
  "articole" : [
    {
      "articol_name" : "Articol 1",
      "value" : 123.54
    },
    {
      "articol_name" : "Articol 2",
      "value" : 123.54
    },
    {
      "articol_name" : "Articol 3",
      "value" : 123.54
    },
    {
      "articol_name" : "Articol 4",
      "value" : 123.54
    }

  ],
  "mode" : [
    {
      "mod_name" : "Cash",
      "value" : 123.54
    },
    {
      "mod_name" : "Card",
      "value" : 123.54
    },
    {
      "mod_name" : "Virament",
      "value" : 123.54
    },
    {
      "mod_name" : "Protocol",
      "value" : 123.54
    }

  ]
}
--------------------------------
/NotaDetails/{partnerId}/{notaId}  //notaId vagy time?
--------------------------------------

{
  "partner_name" : "Nume partener",
  "nota_time" : "2018-12-12 12:12",
  "articole" : [
    {
      "articol_name" : "Articol 1",
      "cantitate" : 12,
      "value" : 123.54
    },
    {
      "articol_name" : "Articol 2",
      "cantitate" : 12,
      "value" : 123.54
    },
    {
      "articol_name" : "Articol 3",
      "cantitate" : 12,
      "value" : 123.54
    },
    {
      "articol_name" : "Articol 4",
      "cantitate" : 12,
      "value" : 123.54
    }

  ]
}


--------------------------------
/OptimizareProfit/{period}/{unitate}/{articolCategoryId}
--------------------------------------

[
    {
      "articol_name" : "Articol 1",
      "articol_category_name" : "Fast Food",
      "vanzare" : 82.00,
      "castig" : 13.00,
      "type" : "PLOWHORSE",
      "vanzare_pret_mediu" : 999.00,
      "vanzare_cantitate" : 890.0,
      "vanzare_valoare_net" : 730.98,
      "castig_pret_mediu" : 430.98,
      "castig_cantitate" : 30.0,
      "castig_valoare_net" : 730.98,
      "materii_prime_pret_mediu" : 8.98,
      "materii_prime_cantitate" : 98.0,
      "materii_prime_valoare_net" : 730.98
    },
    {
      "articol_name" : "Articol 2",
      "vanzare" : 72.00,
      "castig" : 12.23,
      "type" : "STAR",
      "vanzare_pret_mediu" : 999.00,
      "vanzare_cantitate" : 890.0,
      "vanzare_valoare_net" : 730.98,
      "castig_pret_mediu" : 430.98,
      "castig_cantitate" : 30.0,
      "castig_valoare_net" : 730.98,
      "materii_prime_pret_mediu" : 8.98,
      "materii_prime_cantitate" : 98.0,
      "materii_prime_valoare_net" : 730.98
    },
    {
      "articol_name" : "Articol 3",
      "vanzare" : 32.22,
      "castig" : 66.12,
      "type" : "PUZZLE",
      "vanzare_pret_mediu" : 999.00,
      "vanzare_cantitate" : 890.0,
      "vanzare_valoare_net" : 730.98,
      "castig_pret_mediu" : 430.98,
      "castig_cantitate" : 30.0,
      "castig_valoare_net" : 730.98,
      "materii_prime_pret_mediu" : 8.98,
      "materii_prime_cantitate" : 98.0,
      "materii_prime_valoare_net" : 730.98
    },
    {
      "articol_name" : "Articol 4",
      "vanzare" : 62.21,
      "castig" : 50.12,
      "type" : "DOG",
      "vanzare_pret_mediu" : 999.00,
      "vanzare_cantitate" : 890.0,
      "vanzare_valoare_net" : 730.98,
      "castig_pret_mediu" : 430.98,
      "castig_cantitate" : 30.0,
      "castig_valoare_net" : 730.98,
      "materii_prime_pret_mediu" : 8.98,
      "materii_prime_cantitate" : 98.0,
      "materii_prime_valoare_net" : 730.98
    }
]



--------------------------------
/Monitoring
--------------------------------------

[
    {
      "masa" : "65",
      "deschis_la" : "Fast Food",
      "nr_oaspeti" : 6,
      "valoare" : 13.00,
      "ospatar_name" : "Iancu",
      "articole" : [
        {
          "articol_name" : "Articol 1",
          "cantitate" : 15,
          "valoare" : 2.34
        },
        {
          "articol_name" : "Articol 2",
          "cantitate" : 2,
          "valoare" : 2.88
        },
        {
          "articol_name" : "Articol 3",
          "cantitate" : 15,
          "valoare" : 123.8
        },
      ]
    },
    {
      "masa" : "34",
      "deschis_la" : "Fast Food",
      "nr_oaspeti" : 8,
      "valoare" : 143.00,
      "ospatar_name" : "Gezu",
      "articole" : [
        {
          "articol_name" : "Articol 1",
          "cantitate" : 15,
          "valoare" : 2.34
        },
        {
          "articol_name" : "Articol 2",
          "cantitate" : 2,
          "valoare" : 2.88
        },
        {
          "articol_name" : "Articol 3",
          "cantitate" : 15,
          "valoare" : 123.8
        },

      ]
    }
]


*/

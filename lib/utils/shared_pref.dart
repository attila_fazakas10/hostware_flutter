import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  static final String _kServerName = "serverName";
  static final String _kPort = "port";
  static final String _kApiKey = "apiKey";


  static Future<String> getBasicURL() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
//    String serverName = prefs.getString(_kServerName) ?? 'http://chimtarga.asuscomm.com';
//    String port = prefs.getString(_kPort) ?? '880';
    String serverName = prefs.getString(_kServerName) ?? 'http://86.123.108.184';
    String port = prefs.getString(_kPort) ?? '859';
    String apiKey = prefs.getString(_kApiKey) ?? '';
    return serverName+":"+port+"/api/FireBird";
  }
  static Future<String> getServerName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_kServerName) ?? '';
  }
  static Future<bool> setServerName(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_kServerName, value);
  }
  static Future<String> getPort() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_kPort) ?? '';
  }
  static Future<bool> setPort(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_kPort, value);
  }
  static Future<String> getApiKey() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_kApiKey) ?? '';
  }
  static Future<bool> setApiKey(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_kApiKey, value);
  }

}